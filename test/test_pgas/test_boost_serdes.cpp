/**
 * \example hello.cpp
 *
 * Simple "hello world" example
 *
 */

#include <upcxx.h>
#include <iostream>
#include <time.h>

#ifdef CDENABLED

#include <cd.h>
using namespace cd;

using namespace std;
using namespace upcxx;

struct Point {
private:
  friend class boost::serialization::access;
  template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
      ar &a &b;
    }
public:
  int a;
  double b;
  Point():a(-1),b(-1.0){}
  Point(int tmp_a, double tmp_b):a(tmp_a),b(tmp_b){}
};

volatile int num_reexec_hl=0;

int main (int argc, char **argv)
{
  upcxx::init(&argc, &argv);

  Point tmp1(2,3.2);
  Point tmp2(-2,-3.2);

  CDHandle * root_cd = CD_Init(upcxx::ranks(), upcxx::myrank(), kHDD);
  CD_Begin(root_cd);
  //CDHandle * cd_l1 = GetCurrentCD()->Create(upcxx::ranks(), "cd_l1", kRelaxed | kDRAM);
  CDHandle * cd_l1 = GetCurrentCD()->Create(upcxx::ranks(), "cd_l1", kRelaxed | kHDD);
  CD_Begin(cd_l1);

  cd_l1->Preserve<Point>(tmp1,kCopy,"tmp1");
  cd_l1->Preserve<Point>(tmp2,kCopy,"tmp2");

  cout << "tmp1.a=" << tmp1.a << ", tmp1.b=" << tmp1.b << endl;
  cout << "tmp2.a=" << tmp2.a << ", tmp2.b=" << tmp2.b << endl;

  barrier();

  if (num_reexec_hl<1 && (upcxx::myrank()%2==0)){
    printf("injecting #%d error to cd_l1...\n", num_reexec_hl);
    num_reexec_hl++;
    tmp1.a = 4;
    tmp2.b = 5.4;
    cd_l1->CDAssert(false);
  }

  CD_Complete(cd_l1);
  cd_l1->Destroy();
  CD_Complete(root_cd);
  CD_Finalize();

  upcxx::finalize();
  return 0;
}

#endif
