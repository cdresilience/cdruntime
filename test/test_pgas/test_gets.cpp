/*
Copyright 2014, The University of Texas at Austin 
All rights reserved.

THIS FILE IS PART OF THE CONTAINMENT DOMAINS RUNTIME LIBRARY

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met: 

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer. 

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution. 

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdio.h>
#include <iostream>
#include <upcxx.h>

#ifdef CDENABLED
#include "cd.h"
using namespace cd;
//#define PRINT LOG_DEBUG
#define PRINT(...) \
  printf("rank#%d:", myrank()); \
  printf(__VA_ARGS__); \
  fflush(stdout);

#endif

using namespace std;
using namespace upcxx;

FILE *fp;
shared_array< global_ptr<double>> tmp_ptrs;

int main(int argc, char* argv[])
{

  upcxx::init(&argc, &argv);

#ifdef CDENABLED
  volatile int my_num_reexec = 0;
  CDHandle * root = CD_Init(ranks(), myrank());

  CD_Begin(root);
  PRINT("Begin root CD...\n");
#endif

  upcxx::global_ptr<double> ptr1;
  upcxx::global_ptr<double> ptr2;
  upcxx::global_ptr<double> ptr3;
  size_t count = 1024*1024;
  //size_t count = 1024;
  size_t verify_count = 16;

  tmp_ptrs.init(ranks());
  
  uint32_t src_rank = (myrank()+1)%ranks();
  uint32_t dst_rank = myrank();
  ptr1 = allocate<double>(src_rank, count);
  ptr2 = allocate<double>(dst_rank, count);
  cerr << "ptr1::" << ptr1 << "\n";
  cerr << "ptr2::" << ptr2 << "\n";

#ifdef CDENABLED
  CDHandle* child_1 = root->Create(ranks()/*num_children*/, "CD1_0", kRelaxed, 0, 0, NULL);
  CD_Begin(child_1);
  PRINT("CD_Begin child_1...\n");
#endif

  tmp_ptrs[src_rank] = ptr1;

  barrier(); 

  // following line of code just creates a dependency to guarantee writes before barrier finish
  ptr3 = tmp_ptrs[src_rank];

  // Initialize data pointed by ptr by a local pointer
  ptr3 = tmp_ptrs[myrank()];
  cout << "ptr3::" << ptr3 << endl;
  double *local_ptr1 = (double *)ptr3;
  for (size_t i=0; i<count; i++) {
    local_ptr1[i] = (double)i + myrank() * 1e4;
  }

  barrier();
  if (myrank()%2==1){
    upcxx::copy(ptr1, ptr2, count);
  }

  barrier(); 

#ifdef CDENABLED
  if (my_num_reexec == 0 && myrank()%2 == 1)
  {
    //insert data errors
    cout << "Insert error #" << my_num_reexec << " to rank#" << myrank() << "..." << endl;
    cout << myrank() << ": insert errors to data before reexecution:" << endl;

    double *local_ptr2 = (double *)ptr2;
    for (int i=0; i<verify_count; i++) {
      local_ptr2[i]=verify_count-i;
    }

    for (int i=0; i<verify_count; i++) {
      cout << local_ptr2[i] << " ";
    }
    cout << endl;
    fflush(stdout);

    my_num_reexec++;
    child_1->CDAssert(false);
  }

  barrier();

  if (myrank()%2==0){
    upcxx::copy(ptr1, ptr2, count);
  }

  CD_Complete(child_1);
  PRINT("CD_Complete child_1 finishes...\n");
  child_1->Destroy();

#endif

  // barrier before verification
  barrier();

  // verification
  if (myrank()%2==1) {
    cout << "data verification:" << endl;
    fflush(stdout);

    // verify data
    uint32_t src_rank = (myrank()+1)%(ranks());
    double *local_ptr2 = (double *)ptr2;
    double correct_value;
    int i;
    for (i=0; i<verify_count; i++) {
      correct_value = (double)i + src_rank*1e4;
      if (local_ptr2[i]!=correct_value){ 
        cout << "rank#" << myrank() << ": i="<<i<<", local_value::" << local_ptr2[i] << ", correct_value::" << correct_value << endl;
        break;
      }
      //else {
      //  cout << "rank#" << myrank() << ": i="<<i<<", local_value::" << local_ptr2[i] << ", correct_value::" << correct_value << endl;
      //}
    }

    if (i!=verify_count) {
      cout << myrank() << ": !!!ERROR:::SOMETHING WRONG!!!" << endl;
    }
    else {
      printf("verification passed!\n");
    }
    fflush(stdout);
  }

#ifdef CDENABLED
  CD_Complete(root);
  CD_Finalize();
#endif

  // added extra to test deallocation of global ptrs
  upcxx::deallocate(ptr1);
  upcxx::deallocate(ptr2);

  upcxx::finalize();

  return 0;
}

