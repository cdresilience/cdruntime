/**
 * \example hello.cpp
 *
 * Simple "hello world" example
 *
 */

#include <upcxx.h>
#include <iostream>
#include <time.h>

#ifdef CDENALBED
#include <cd.h>
using namespace cd;
#endif

using namespace std;
using namespace upcxx;

int main (int argc, char **argv)
{
  volatile int num_reexec=0;
  upcxx::init(&argc, &argv);
  int myrank = upcxx::myrank();
  int ranks = upcxx::ranks();
  int a=myrank;

#ifdef CDENABLED
  CDHandle * root_cd = CD_Init(ranks, myrank);
  CD_Begin(root_cd);
  root_cd->Preserve(&a, sizeof(a), kCopy, "a");

  CDHandle * cd_l1 = GetCurrentCD()->Create(2, "cd_l1", kStrict, 0, 0, NULL);
  CD_Begin(cd_l1);

  cd_l1->Preserve(&a, sizeof(a), kCopy, "a");

  CDHandle * cd_l2 = GetCurrentCD()->Create(ranks/2, "cd_l2", kRelaxed, 0, 0, NULL);
  CD_Begin(cd_l2);
#endif

  cout << myrank << "before increment: a="<<a<<endl;
  a=myrank+1;
  cout << myrank << "after increment: a="<<a<<endl;
  fflush(stdout);

#ifdef CDENABLED
  CD_Complete(cd_l2);
  cd_l2->Destroy();

  if (num_reexec==0 && myrank==1){
    std::cout << "rank " << myrank << " injects error #" << num_reexec << std::endl;
    std::cout << std::endl;
    fflush(stdout);
    num_reexec++;
    cd_l1->CDAssert(false);
  }

  CD_Complete(cd_l1);
  cd_l1->Destroy();

  //upcxx::barrier();
  CD_Complete(root_cd);
  CD_Finalize();
#endif

  upcxx::finalize();
  return 0;
}
