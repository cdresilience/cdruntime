/*
Copyright 2014, The University of Texas at Austin 
All rights reserved.

THIS FILE IS PART OF THE CONTAINMENT DOMAINS RUNTIME LIBRARY

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met: 

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer. 

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution. 

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdio.h>
#include <iostream>
#include <upcxx.h>

#ifdef CDENABLED
#include "cd.h"
using namespace cd;
#endif

using namespace std;
using namespace upcxx;

FILE *fp;
shared_array< global_ptr<double>> tmp_ptrs;

int main(int argc, char* argv[])
{

  upcxx::init(&argc, &argv);
  volatile int my_num_reexec = 0;

#ifdef CDENABLED
  CDHandle * root = CD_Init(ranks(), myrank());
  std::cout << "I'm thread " << upcxx::myrank() << " of " << upcxx::ranks() << " threads \n";
  if (myrank()==0)
    std::cout << "max_am_payload_size()=" << max_am_payload_size() << std::endl;

  CD_Begin(root);
  LOG_DEBUG("Begin root CD...\n");
#endif

  // User-defined Create() example.
  int color = myrank();
  int task_in_color = 0;
  int color_num = ranks();

  upcxx::global_ptr<double> ptr1;
  upcxx::global_ptr<double> ptr2;
  upcxx::global_ptr<double> ptr3;
  size_t count = 20;
  size_t verify_count = 16;

  tmp_ptrs.init(ranks());
  
#ifdef CDENABLED
  LOG_DEBUG("CD_Begin child_1...\n");
  //CDHandle* child_1 = root->Create(color, task_in_color, color_num/*num_children*/, "CD1_0", kRelaxed, 0, 0, NULL);
  CDHandle* child_1 = root->Create(color_num/*num_children*/, "CD1_0", kRelaxed, 0, 0, NULL);
  CD_Begin(child_1);
#endif

  uint32_t dst_rank = (myrank()+1)%ranks();

  {
    ptr1 = allocate<double>(myrank(), count);
    ptr2 = allocate<double>(dst_rank, count);

    cerr << "ptr1 " << ptr1 << "\n";
    cerr << "ptr2 " << ptr2 << "\n";
    tmp_ptrs[dst_rank] = ptr2;

    // Initialize data pointed by ptr by a local pointer
    double *local_ptr1 = (double *)ptr1;
    for (size_t i=0; i<count; i++) {
      local_ptr1[i] = (double)i + myrank() * 1e4;
    }

    upcxx::copy(ptr1, ptr2, count);
  }

  barrier();

#ifdef CDENABLED
  ////LOG_DEBUG("CD_Complete child_1...\n");
  ////CD_Complete(child_1);

  ////LOG_DEBUG("CD_Begin child_1 second time...\n");
  ////CD_Begin(child_1);
#endif

  // added extra to test deallocation of global ptrs
  upcxx::deallocate(ptr1);
  upcxx::deallocate(ptr2);

  {
    ptr3 = tmp_ptrs[myrank()];
    // verify data
    cout << myrank() << ": data verification:" << endl;
    double *local_ptr2 = (double *)ptr3;
    for (int i=0; i<verify_count; i++) {
      cout << local_ptr2[i] << " ";
    }
    cout << endl;

  }

#ifdef CDENABLED
  //insert error
  if (my_num_reexec == 0 && myrank()%2 == 0)
  {
    ptr3 = tmp_ptrs[myrank()];
    cout << myrank() << ": delibrately damage data before reexecution:" << endl;
    double *local_ptr2 = (double *)ptr3;
    for (int i=0; i<verify_count; i++) {
      local_ptr2[i]=verify_count-i;
      cout << local_ptr2[i] << " ";
    }
    cout << endl;

    std::cout << "Insert error #" << my_num_reexec << "..." << std::endl;
    my_num_reexec++;
    std::cout << "my_num_reexec = " << my_num_reexec << " after increment..." << std::endl;
    child_1->CDAssert(false);
    //root->CDAssert(false);
  }

  //barrier();
  LOG_DEBUG("CD_Complete child_1...\n");
  CD_Complete(child_1);
  child_1->Destroy();

  //barrier();
  LOG_DEBUG("Before complete root CD...\n");
  CD_Complete(root);
  LOG_DEBUG("Before CD_Finalize()...\n");

  CD_Finalize();
#endif

  if (myrank() == 0)
    printf("test_global_ptr passed!\n");

  upcxx::finalize();

  return 0;
}

