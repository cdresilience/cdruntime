/**
 * \example hello.cpp
 *
 * Simple "hello world" example
 *
 */

#include <upcxx.h>
#include <iostream>
#include <time.h>

#ifdef CDENABLED
#include <cd.h>
using namespace cd;
#endif

#define PRINT(...) \
  printf("rank#%d:", myrank()); \
  printf(__VA_ARGS__); \
  fflush(stdout);

using namespace std;
using namespace upcxx;

int main (int argc, char **argv)
{
  volatile int num_reexec=0;
  upcxx::init(&argc, &argv);
  int my_rank = upcxx::myrank();
  int ranks = upcxx::ranks();
  clock_t start = clock();
  clock_t end;

#ifdef CDENABLED
  CDHandle * root_cd = CD_Init(ranks, my_rank);
  CD_Begin(root_cd);
#endif

  barrier();
  end = clock();
  PRINT("at %f seconds.\n", (end-start)/(double)CLOCKS_PER_SEC);

  // computations to occupy time..
  if (my_rank == 0){
    PRINT("some computation to occupy time...\n");
  }
  size_t res=0;
  for (size_t i=0; i<1024; i++) {
    for (size_t j=0; j<1024*1024; j++) {
      res += i*j;
    }
  }

#ifdef CDENABLED
  if (num_reexec == 0 && my_rank == 2){
    end = clock();
    PRINT("injects error #%d at time::%f\n", num_reexec, (end-start)/(double)CLOCKS_PER_SEC);
    num_reexec++;
    root_cd->CDAssert(false);
  }
#endif

#ifdef CDENABLED
  CD_Complete(root_cd);
  CD_Finalize();
#endif

  upcxx::finalize();
  return 0;
}
