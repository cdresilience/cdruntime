/**
 * \example test_async_copy.cpp
 *
 * Test async_copy with/without explicit events
 * + test initiation/sync within same CD
 * + test initiation/sync not within same CD
 * + test multiple implicit event async_copy within same CD
 *
 */

/* this tests read logs only...*/

#include <upcxx.h>
#include <iostream>
#include <stdio.h>

#ifdef CDENABLED
#include "cd.h"
using namespace cd;
#endif

using namespace upcxx;
using namespace std;

#ifdef CDENABLED
  #define PRINT LOG_DEBUG
  //#define PRINT(...) \
  //  {\
  //  printf("Rank#%d:", myrank()); \
  //  printf(__VA_ARGS__); \
  //  fflush(stdout); \
  //  }
#else
  #define PRINT printf
#endif

void computation(size_t N=1024){
  PRINT("In computation..\n");
  size_t res=0;
  for (size_t i=0; i<N; i++) {
    for (size_t j=0; j<N*N; j++) {
      res += i*j;
    }
  }
}

void verify_results(event *e, global_ptr<double> src, global_ptr<double> dst, size_t sz)
{
  if (e!=NULL){
    assert(e->isdone());
  }
  if (src[sz-1].get()!=dst[sz-1].get()){
    PRINT("\x1b[31m !!Verification failed, src[%ld]=%f, dst[%ld]=%f...\x1b[0m\n", sz-1, src[sz-1].get(), sz-1, dst[sz-1].get());
    ERROR_MESSAGE("\x1b[31m !!Verification failed, src[%ld]=%f, dst[%ld]=%f...\x1b[0m\n", sz-1, src[sz-1].get(), sz-1, dst[sz-1].get());
    exit(0);
  }
  else {
    PRINT("\x1b[32m !!Verification passed, src[%ld]=dst[%ld]=%f...\x1b[0m\n", sz-1, sz-1, src[sz-1].get());
  }
}

int main(int argc, char **argv)
{
  upcxx::init(&argc, &argv);
  if (upcxx::ranks()%2){
    if (upcxx::myrank()==0){
      ERROR_MESSAGE("Need even number ranks for this test!!\n");
      //std::cout << "\nERROR: Need even number ranks for this test!! \n" << std::endl;
    }
    upcxx::finalize();
    return 1;
  }

  global_ptr<double> src, dst;
  global_ptr<double> src1, dst1;
  global_ptr<double> src2, dst2;

  event copy_e, copy_e_2;

  size_t sz = 1024*1024;
  //size_t sz = 1024;
  src = allocate<double>((myrank()+1)%ranks(), sz);
  src1 = allocate<double>(myrank(), sz);
  src2 = allocate<double>((myrank()+2)%ranks(), sz);
#ifdef UPCXX_HAVE_CXX11
  assert(src != nullptr); // assert(src != NULL); is OK but not type-safe
#else  
  assert(src.raw_ptr() != NULL); // or assert(!src.isnull());
#endif

  dst = allocate<double>(myrank(), sz);
  dst1 = allocate<double>((myrank()+1)%ranks(), sz);
  dst2 = allocate<double>((myrank()+1)%ranks(), sz);

#ifdef UPCXX_HAVE_CXX11
  assert(dst != nullptr); // assert(dst != NULL); is OK but not type-safe
#else
  assert(dst.raw_ptr() != NULL); // or assert(!dst.isnull());
#endif
  
#ifdef CDENABLED
  volatile int num_reexec=0;
  volatile int num_reexec_hl=0;

  //cout << myrank()<<":src::" << src << "\n";
  //cout << myrank()<<":dst::" << dst << "\n";
  //cout << myrank()<<":src1::" << src1 << "\n";
  //cout << myrank()<<":dst1::" << dst1 << "\n";
  //cout << myrank()<<":src2::" << src2 << "\n";
  //cout << myrank()<<":dst2::" << dst2 << "\n";

  CDHandle * root_cd = CD_Init(upcxx::ranks(), upcxx::myrank());
  CD_Begin(root_cd);
#endif

#ifdef CDENABLED
  // create and begin level 1 CD...
  CDHandle *cd_l1 = GetCurrentCD()->Create(2, "cd_l1", kRelaxed, 0, 0, NULL);
  CD_Begin(cd_l1);
#endif

#ifdef CDENABLED
  // create and begin level 2 CD...
  CDHandle *cd_l2 = GetCurrentCD()->Create(upcxx::ranks()/2, "cd_l2", kRelaxed, 0, 0, NULL);
#endif

#ifdef CDENABLED
  PRINT("*******************************************************************\n");
  PRINT("...Test for async_copy (gasnet_get_nb_bulk) with explicit events...\n");
  PRINT("*******************************************************************\n\n\n");
  CD_Begin(cd_l2);
#endif
  
#ifdef UPCXX_HAVE_CXX11
  assert(dst != nullptr); // assert(dst != NULL); is OK but not type-safe
#else  
  assert(dst.raw_ptr() != NULL); // or assert(!dst.isnull());
#endif
  src[sz-1] = 123456.789+(double)myrank();
  dst[sz-1] = 1.0;
  
  barrier();
  async_copy(src, dst, sz, &copy_e);
  PRINT("Step 1.5 right after async_copy, src[%lu] = %f, dst[%lu] = %f\n",
         sz-1, src[sz-1].get(), sz-1, dst[sz-1].get());
  
  copy_e.wait();
  verify_results(&copy_e, src, dst, sz);
  
#ifdef CDENABLED
  if (myrank()==1 && num_reexec<=10){
    PRINT("injecting #%d error to cd_l2...\n", num_reexec);
    num_reexec++;
    cd_l2->CDAssert(false);
  }
  CD_Complete(cd_l2);
#endif

#ifdef CDENABLED
  PRINT("*****************************************************************************\n");
  PRINT("...Test for async_copy (gasnet_put_nb_bulk) with explicit events 2nd time!...\n");
  PRINT("*****************************************************************************\n\n\n");
  CD_Begin(cd_l2);
#endif
  
  // in second run, data is formatted as 2xxxx.xxxx
  src1[sz-1] = 22345.6789+(double)myrank();
  dst1[sz-1] = 2.0;
  
  barrier();
  async_copy(src1, dst1, sz, &copy_e);
  PRINT("Step 1.5 right after async_copy, src1[%lu] = %f, dst1[%lu] = %f\n",
         sz-1, src1[sz-1].get(), sz-1, dst1[sz-1].get());
  
  copy_e.wait();
  verify_results(&copy_e, src1, dst1, sz);
  
#ifdef CDENABLED
  CD_Complete(cd_l2);
#endif

#ifdef CDENABLED
  PRINT("***********************************************************************\n");
  PRINT("...Test for async_copy (gasnet_get_nbi_bulk) without explicit events...\n");
  PRINT("***********************************************************************\n\n\n");
  CD_Begin(cd_l2);
#endif
  //barrier();
  assert(dst != nullptr);
  src[sz-1] = 987654.321+(double)myrank();
  dst[sz-1] = 0.0;
  barrier();

  //PRINT("Step 1 right before async_copy (no event), src[%lu] = %f, dst[%lu] = %f\n",
  //       sz-1, src[sz-1].get(), sz-1, dst[sz-1].get());
  async_copy(src, dst, sz);
  PRINT("Step 1.5 right after async_copy (no event), src[%lu] = %f, dst[%lu] = %f\n",
         sz-1, src[sz-1].get(), sz-1, dst[sz-1].get());

  async_copy_fence();
  verify_results(NULL, src, dst, sz);

#ifdef CDENABLED
  if (num_reexec<=5 && myrank()==0){
    PRINT("injecting #%d error to cd_l2...\n", num_reexec);
    num_reexec++;
    cd_l2->CDAssert(false);
  }
  CD_Complete(cd_l2);
#endif

#ifdef CDENABLED
  PRINT("***************************************************************************\n");
  PRINT("...Test for pack/push logs for nb/nbi communication (put_nbi_bulk, etc.)...\n");
  PRINT("***************************************************************************\n\n\n");

  CD_Begin(cd_l2);
  //barrier();

  // create and begin level 3 CD...
  CDHandle *cd_l3 = GetCurrentCD()->Create("cd_l3", kRelaxed, 0, 0, NULL);
  CD_Begin(cd_l3);
#endif

  assert(dst != nullptr);
  src[sz-1] = 98765.4321+(double)myrank();
  dst[sz-1] = 0.0; 
  src1[sz-1] = 987.654321+(double)myrank();
  dst1[sz-1] = 0.0;
  src2[sz-1] = 98.7654321+(double)myrank();
  dst2[sz-1] = 0.0;
  barrier();

  // nbi comm
  async_copy(src1, dst1, sz);
  PRINT("Step 1.5 right after async_copy (no event), src1[%lu] = %f, dst1[%lu] = %f\n",
         sz-1, src1[sz-1].get(), sz-1, dst1[sz-1].get());

  // 2nd nbi comm 
  copy(src2, dst2, sz);
  verify_results(NULL, src2, dst2, sz);
  PRINT("Step 1.5 right after async_copy (no event), src2[%lu] = %f, dst2[%lu] = %f\n",
         sz-1, src2[sz-1].get(), sz-1, dst2[sz-1].get());

  // nb comm
  async_copy(src, dst, sz, &copy_e);
  PRINT("Step 1.5 right after async_copy, src[%lu] = %f, dst[%lu] = %f\n",
         sz-1, src[sz-1].get(), sz-1, dst[sz-1].get());

#ifdef CDENABLED
  // complete level 3 CD...
  CD_Complete(cd_l3);
  cd_l3->Destroy();
  PRINT("before cd_l2 complete..\n");
  CD_Complete(cd_l2);
#endif

#ifdef CDENABLED
  CD_Begin(cd_l2);
  // create and begin level 3 CD...
  cd_l3 = GetCurrentCD()->Create("cd_l3", kRelaxed, 0, 0, NULL);
  CD_Begin(cd_l3);
#endif

  // nb comm fence 
  copy_e.wait();
  verify_results(&copy_e, src, dst, sz);
  PRINT("Step 2 after e.wait, src[%lu] = %f, dst[%lu] = %f\n",
         sz-1, src[sz-1].get(), sz-1, dst[sz-1].get());
  
  // nbi comm fence
  async_copy_fence();
  verify_results(NULL, src1, dst1, sz);

#ifdef CDENABLED
  CD_Complete(cd_l3);
  cd_l3->Destroy();

  CD_Complete(cd_l2);
#endif

#ifdef CDENABLED
  cd_l2->Destroy();
#endif

#ifdef CDENABLED
  PRINT("*************************************\n");
  PRINT("...Test for higher level rollbacks...\n");
  PRINT("*************************************\n\n\n");

  if (num_reexec_hl<=1 && (myrank()%2==0)){
    PRINT("injecting #%d error to cd_l1...\n", num_reexec_hl);
    num_reexec_hl++;
    cd_l1->CDAssert(false);
  }
#endif

#ifdef CDENABLED
  CD_Complete(cd_l1);
  cd_l1->Destroy();
#endif

#ifdef CDENABLED
  // error injection for root_cd
  if (num_reexec_hl==2 && myrank()==0){
    PRINT("injecting #%d error to root_cd...\n", num_reexec_hl);
    num_reexec_hl++;
    root_cd->CDAssert(false);
  }
#endif

#ifdef CDENABLED
  barrier();

  // log profiling
  if (myrank()==0){
    printf("**********************************************\n");
    printf("... Log Profiling ...\n");
    printf("**********************************************\n");
  }
  barrier();
  double num_log_entry;
  double log_volume;
  root_cd->LogProfilingReport(&num_log_entry, &log_volume);
  printf("Rank#%d::log profiling reports: num_log_entry=%f, and tot_log_volume=%f\n", myrank(), num_log_entry, log_volume);

  double total_num_log_entry, total_log_volume;
  upcxx_reduce<double>(&num_log_entry, &total_num_log_entry, 1, 0, UPCXX_SUM, UPCXX_DOUBLE);
  upcxx_reduce<double>(&log_volume, &total_log_volume, 1, 0, UPCXX_SUM, UPCXX_DOUBLE);
  if (myrank() == 0){
    printf("Total log profiling reports: num_log_entry=%f, and tot_log_volume=%f\n\n", 
        total_num_log_entry, total_log_volume);
  }
#endif

#ifdef CDENABLED
  CD_Complete(root_cd);
  CD_Finalize();
#endif

  upcxx::finalize();

  return 0;
}
