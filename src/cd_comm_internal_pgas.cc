/*
Copyright 2014, The University of Texas at Austin 
All rights reserved.

THIS FILE IS PART OF THE CONTAINMENT DOMAINS RUNTIME LIBRARY

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met: 

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer. 

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution. 

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/

#include "cd_features.h"

#if CD_PGAS_ENABLED

#include "cd_def_internal.h"
#include "cd_handle.h"
#include "cd_internal.h"
#include "cd_global.h"
#include "upcxx/atomic_for_cd.h"
#include "upcxx/team_for_cd.h"


using namespace std;
using namespace cd;
using namespace cd::internal;
//using namespace upcxx;

namespace cd {
NodeID CDHandle::GenNewNodeID(ColorT &my_color, const int &new_color, const int &new_task, int new_head_id, bool is_reuse)
{
  NodeID new_node_id(new_head_id);
  my_color.split((uint32_t)new_color, (uint32_t)new_task, new_node_id.pcolor_);

  new_node_id.size_=new_node_id.color().size();
  new_node_id.task_in_color_=new_node_id.color().myrank();

  return new_node_id;
}

// Collect child CDs' head information.
void CDHandle::CollectHeadInfoAndEntry(const NodeID &new_node_id){
  // Get the children CD's head information and the total size of entry from each task in the CD.
  int send_buf[2]={0,0};
  int task_count = node_id().size();
  int recv_buf[task_count][2]; 
  if(new_node_id.IsHead()) {
    send_buf[0] = node_id().task_in_color();
  } else {
    send_buf[0] = -1;
  }
  send_buf[1] = ptr_cd()->remote_entry_directory_map_.size();

  node_id().color().allgather(send_buf, recv_buf, 2*sizeof(int));

  CD_DEBUG("\n================== Remote entry check ===================\n");
  CD_DEBUG("[Before] Check entries in remote entry directory\n");

  for(auto it = ptr_cd()->remote_entry_directory_map_.begin();
           it!= ptr_cd()->remote_entry_directory_map_.end(); ++it) {
    CD_DEBUG("%s\n", it->second->GetString().c_str());

  }
  uint64_t serialized_len_in_bytes=0;

  void *serialized_entry = ptr_cd()->SerializeRemoteEntryDir(serialized_len_in_bytes); 
  CD_DEBUG("CollectHeadInfoAndEntry: need to transfer total %d bytes of data.\n", serialized_len_in_bytes);

  // if there is no entries to send to head, just return
  if(serialized_entry == NULL) return;
  
  int recv_count = task_count*serialized_len_in_bytes;
  //  int recv_count = task_count*stride;
  void *entry_to_deserialize = NULL;

  entry_to_deserialize = malloc(serialized_len_in_bytes);

  CD_DEBUG("\n\nNote : %p, %u, %d, remote entry dir map size : %lu\n\n", 
           serialized_entry, serialized_len_in_bytes, recv_count, 
           ptr_cd()->remote_entry_directory_map_.size());

  node_id().color().gather(serialized_entry, entry_to_deserialize, serialized_len_in_bytes, node_id().head());

  if(IsHead()) {
    void * temp_ptr = (void *)((char *)entry_to_deserialize+serialized_len_in_bytes-8);

    CD_DEBUG("Check it out : %p -- %p, diff : %p\n", 
             entry_to_deserialize, temp_ptr, (void *)((char *)temp_ptr - (char *)entry_to_deserialize));

    ptr_cd()->DeserializeRemoteEntryDir(ptr_cd()->remote_entry_directory_map_, entry_to_deserialize, task_count, serialized_len_in_bytes); 

    CD_DEBUG("\n\n[After] Check entries after deserialization, size : %lu, # of tasks : %u, level : %u\n", 
             ptr_cd()->remote_entry_directory_map_.size(), node_id_.size(), ptr_cd()->GetCDID().level());

    CD_DEBUG("\n\n============================ End of deserialization ===========================\n\n");
  }

  for(auto it=ptr_cd()->remote_entry_directory_map_.begin(); it!=ptr_cd()->remote_entry_directory_map_.end(); ++it) {
    CD_DEBUG("%s\n", it->second->GetString().c_str());
  }  

  // free temporary space used for (de)serialization
  free(entry_to_deserialize);

  return;
}

// Synchronize every task of this CD.
CDErrT CDHandle::Sync(ColorT color){
  ptr_cd()->Sync(color);
  return kOK;
}

//inline
void CD::UnsetEventFlag(CDFlagT event_mask) {
  CDFlagT tmp_event = (event_flag_.raw_ptr())->load();
  tmp_event &= ~event_mask;
  (event_flag_.raw_ptr())->store(tmp_event);
}

//inline
void HeadCD::UnsetEventFlag(int task_id, CDFlagT event_mask) {
  CDFlagT tmp_event = (event_flag_.raw_ptr())[task_id].load();
  tmp_event &= ~event_mask;
  (event_flag_.raw_ptr())[task_id].store(tmp_event);
}

//TODO:
// 1) atomically increment pending request count at head side
// 2) atomically set event flag at head side??
CD::CDInternalErrT CD::RemoteSetMailBox(const CDEventT &event){
  CD_DEBUG("[CD::RemoteSetMailBox] event : %s at %s / %s\n", 
           event2str(event).c_str(), 
           GetCDName().GetString().c_str(), 
           GetNodeID().GetString().c_str());

  CDInternalErrT ret=kOK;

  int head_id = head();

  if(event != CDEventT::kNoEvent) {
    CD_DEBUG("Set CD Event %s at level #%u. CD Name %s\n", event2str(event).c_str(), level(), GetCDName().GetString().c_str());

    // Increment pending request count at the target task (requestee)
    upcxx::fetch_add_for_cd((pendingFlag_[task_in_color_l2g_[head_id]]).get(), 1);
    CD_DEBUG("head_id=%d, and its global task id=%d.\n", head_id, task_in_color_l2g_[head_id]);
    CD_DEBUG("Increment pending counter done for task head #%u\n", head_id);
  
    upcxx::bor_for_cd(head_event_flag_, event);
    //CDFlagGPtrT target_global_ptr = head_event_flag_;
    //CDFlagT tmp_event=target_global_ptr[task_in_color()].get().load();
    //tmp_event&=event;
    //target_global_ptr[task_in_color()].get().store(tmp_event);
  }
  CD_DEBUG("[CD::RemoteSetMailBox] PGAS Version Done.\n");

  return ret;
}

CDErrT HeadCD::SetMailBox(const CDEventT &event, int task_id){
  CD_DEBUG("\n\n=================== Set Mail Box (%s, %d) Start! myTaskID #%d ==========================\n", 
      event2str(event).c_str(), task_id, myTaskID);
 
  if(event == CDEventT::kErrorOccurred || event == CDEventT::kEntrySearch) {
    //ERROR_MESSAGE("[HeadCD::SetMailBox(event, task_id)] Error, the event argument is something wrong. event: %s\n", event2str(event).c_str());
    ERROR_MESSAGE("[HeadCD::SetMailBox(event, task_id)] Error, the event argument is something wrong. \n");
  }

  if(task_size() == 1) {
    ERROR_MESSAGE("[HeadCD::SetMailBox(event, task_id)] Error, # of tasks should be greater than 1, but # of tasks is 1\n");
  }

  CDErrT ret=CDErrT::kOK;
  int val = 1;
  if(event == CDEventT::kNoEvent) {
    // There is no vent to set.
    CD_DEBUG("No event to set\n");
  }
  else {
    if(task_id != task_in_color()) {
 
      // Increment pending request count at the target task (requestee)
      if(event != CDEventT::kNoEvent) {
        CD_DEBUG("Set CD Event %s at level #%u. CD Name : %s\n", event2str(event).c_str(), level(), GetCDName().GetString().c_str());
        CD_DEBUG("Accumulate event at %d\n", task_id);
  
        upcxx::fetch_add_for_cd(pendingFlag_[task_in_color_l2g_[task_id]].get(), 1);
        CD_DEBUG("task_id=%d, and its global task id=%d.\n", task_id, task_in_color_l2g_[task_id]);
        CD_DEBUG("Increment pending counter done for task #%u\n", task_id);
    
        if(task_id == task_in_color()) { 
          CD_DEBUG("after accumulate --> pending counter : %ld\n", (pendingFlag_[upcxx::myrank_for_cd()].get())->load());
        }
        
        upcxx::bor_for_cd(nonhead_event_flag_[task_id], event);
      }

      CD_DEBUG("SetMailBox done for task #%d\n", task_id);
  
      if(task_id == task_in_color()) { 
        CD_DEBUG("after accumulate --> event : %s\n", event2str(event_flag_[task_id].get().load()).c_str());
      }
    }
    else {
      // If the task to set event is the head itself,
      // it does not increase pending counter and set event flag through RDMA,
      // but it locally increase the counter and directly register event handler.
      // The reason for this is that the head task's CheckMailBox is scheduled before non-head task's,
      // and after head's CheckMailBox associated with SetMailBox for some events such as kErrorOccurred,
      // (kErrorOccurred at head tast associates kAllReexecute for all task in the CD currently)
      // head CD does not check mail box to invoke kAllRexecute for itself. 
      // Therefore, it locally register the event handler right away, 
      // and all the tasks including head task can reexecute after the CheckMailBox.
      CD_DEBUG("\nEven though it calls SetMailBox(%s, %d == %u), locally set/handle events\n", 
               event2str(event).c_str(), task_id, task_in_color());

      CDInternalErrT cd_ret = LocalSetMailBox(event);
      ret = static_cast<CDErrT>(cd_ret);
    }
  }

  CD_DEBUG("\n=================== Set Mail Box Done ==========================\n");

  return ret;
}


// SZ: output functions for DEGAS Demo
std::string CDHandle::PrintLeftBoundary(const int &cur_level){
  string tmp_string="";
  if (!GetParent()){
    if (level()!=cur_level) {
      if (task_in_color()==0) 
        tmp_string += "|";
      else 
        tmp_string += " ";
    }
    return tmp_string;
  }

  tmp_string += GetParent()->PrintLeftBoundary(cur_level);
  if (level()!=cur_level){
    if (task_in_color()==0) 
      tmp_string += "|";
    else 
      tmp_string += " ";
  }
  return tmp_string;
}

std::string CDHandle::PrintRightBoundary(const int &cur_level){
  string tmp_string="";
  if (!GetParent()){
    if (level()!=cur_level){
      if (task_in_color()==(task_size()-1)) 
        tmp_string += "|";
      else 
        tmp_string += " ";
    }
    return tmp_string;
  }

  if (level()!=cur_level){
    if (task_in_color()==(task_size()-1)) 
      tmp_string += "|";
    else 
      tmp_string += " ";
  }
  tmp_string += GetParent()->PrintRightBoundary(cur_level);

  return tmp_string;
}

// this function can only be called by leaf CD...
void CDHandle::PrintIteration(FILE* foutput, int out_iter, int iteration){
  string tmp_string="";
  if (task_size()==1){
    tmp_string += PrintLeftBoundary(level());
    tmp_string += "|  iter ("+std::to_string(out_iter)+",";
    if (iteration<=9)
      tmp_string += "0"+std::to_string(iteration);
    else
      tmp_string += std::to_string(iteration);
    tmp_string += ")  |";
    tmp_string += PrintRightBoundary(level());

    fprintf(foutput, "%s\n", tmp_string.c_str());
    fflush(foutput);
    return;
  }
  else {
    tmp_string += PrintLeftBoundary(level());

    if (task_in_color()==0) tmp_string += "| ";
    else tmp_string += "  ";

    tmp_string += " iter ("+std::to_string(out_iter)+",";
    if (iteration<=9)
      tmp_string += "0"+std::to_string(iteration);
    else
      tmp_string += std::to_string(iteration);
    tmp_string += ") ";
    
    if (task_in_color()==task_size()-1) tmp_string += " |";
    else tmp_string += "  ";
    tmp_string += PrintRightBoundary(level());

    fprintf(foutput, "%s\n", tmp_string.c_str());
    fflush(foutput);
    return;
  }
}

void CDHandle::PrintBegin(FILE *foutput) {
  string tmp_string="";

  // print begin information
  if (task_size()==1){
    tmp_string += PrintLeftBoundary(level());
    tmp_string += "|++L"+std::to_string(level())+" CD Begin++|";
    tmp_string += PrintRightBoundary(level());

    fprintf(foutput, "%s\n", tmp_string.c_str());
    fflush(foutput);
  }
  else {
    if (task_in_color()==0){
      tmp_string += PrintLeftBoundary(level());
      tmp_string += "|++";
      tmp_string += " L"+std::to_string(level())+" CD Begin++";
      if (level()==0) tmp_string += "++++";
      else if (level()==1) tmp_string += "++";
      tmp_string += PrintRightBoundary(level());
    }
    else if (task_in_color()==task_size()-1){
      tmp_string += PrintLeftBoundary(level());
      tmp_string += "+++++++++++++++";
      if (level()==0) tmp_string += "++++";
      else if (level()==1) tmp_string += "++";
      tmp_string += "+|";
      tmp_string += PrintRightBoundary(level());
    }
    else {
      tmp_string += PrintLeftBoundary(level());
      tmp_string += "+++++++++++++++++";
      if (level()==0) tmp_string += "++++";
      else if (level()==1) tmp_string += "++";
      tmp_string += PrintRightBoundary(level());
    }

    fprintf(foutput, "%s\n", tmp_string.c_str());
    fflush(foutput);
  }
}

void CDHandle::PrintFinalEmptyLine(FILE *foutput){
  std::string tmp_string = "    ";
  fprintf(foutput, "%s\n", tmp_string.c_str());
  fflush(foutput);
}

void CDHandle::PrintComplete(FILE *foutput) {
  string tmp_string="";
  if (task_size()==1){
    // print complete information
    tmp_string += PrintLeftBoundary(level());
    tmp_string += "|-L"+std::to_string(level())+"CD Complete-|";
    tmp_string += PrintRightBoundary(level());
    tmp_string += "\n";

    // print an empty line
    tmp_string += PrintLeftBoundary(level());
    tmp_string += "                 ";
    if (level()==0) tmp_string += "    ";
    else if (level()==1) tmp_string += "  ";
    tmp_string += PrintRightBoundary(level());

    fprintf(foutput, "%s\n", tmp_string.c_str());
    fflush(foutput);
  }
  else {
    //tmp_string += PrintEmptyLine();
    if (task_in_color()==0){
      tmp_string += PrintLeftBoundary(level());
      tmp_string += "|-";
      tmp_string += "L"+std::to_string(level())+" CD Complete-";
      if (level()==0) tmp_string += "----";
      else if (level()==1) tmp_string += "--";
      tmp_string += PrintRightBoundary(level());
    }
    else if (task_in_color()==task_size()-1){
      tmp_string += PrintLeftBoundary(level());
      if (level()==0) tmp_string += "----";
      else if (level()==1) tmp_string += "--";
      tmp_string += "---------------";
      tmp_string += "-|";
      tmp_string += PrintRightBoundary(level());
    }
    else {
      tmp_string += PrintLeftBoundary(level());
      if (level()==0) tmp_string += "----";
      else if (level()==1) tmp_string += "--";
      tmp_string += "-----------------";
      tmp_string += PrintRightBoundary(level());
    }
    tmp_string += "\n";

    // print an empty line
    tmp_string += PrintLeftBoundary(level());
    tmp_string += "                 ";
    if (level()==0) tmp_string += "    ";
    else if (level()==1) tmp_string += "  ";

    tmp_string += PrintRightBoundary(level());

    fprintf(foutput, "%s\n", tmp_string.c_str());
    fflush(foutput);
  }
}

void CDHandle::PrintInjectError(FILE* foutput, int err_num){
  string tmp_string="";
  if (task_size()==1){

    // print error information
    tmp_string += PrintLeftBoundary(level());
    tmp_string += "|  ";
    if (err_num<=9)
      tmp_string += " ERROR #0"+std::to_string(err_num)+" ";
    else
      tmp_string += " ERROR #"+std::to_string(err_num)+" ";
    tmp_string += "  |";
    tmp_string += PrintRightBoundary(level());

    fprintf(foutput, "%s\n", tmp_string.c_str());
    fflush(foutput);
  }
}

std::string CDHandle::PrintEmptyLine(void){
  std::string tmp_string="";
  tmp_string += PrintLeftBoundary(level());
  // left boundary for myself
  if (task_in_color()==0) tmp_string += "| ";
  else tmp_string += "  ";

  tmp_string += "             ";
  if (level()==0) tmp_string += "    ";
  else if (level()==1) tmp_string += "  ";

  // right boundary for my self
  if (task_in_color() == task_size()-1) tmp_string += " |";
  else tmp_string += "  ";
  tmp_string += PrintRightBoundary(level());
  tmp_string += "\n";

  return tmp_string;
}

void CDHandle::PrintInjectError(FILE* foutput, int err_num, int global_thread_id){
  if (task_size()==1) {
    PrintInjectError(foutput, err_num);
    return;
  }
  // eleminate all ranks that are not in failed CD..
  bool need_print=false;
  for (int ii=0; ii<task_size(); ii++){
    if (ptr_cd()->GetGlobalTaskInColor(ii) == global_thread_id){
      need_print = true;
      break;
    }
  }
  
  if (need_print) {
    std::string tmp_string = "";
    //// print empty line before
    //tmp_string += PrintEmptyLine();

    // print error injection..
    // non-failed rank print empty line
    if (upcxx::myrank_for_cd() == global_thread_id){
      tmp_string += PrintLeftBoundary(level());
      // left boundary for myself
      if (task_in_color()==0) tmp_string += "| ";
      else tmp_string += "  ";

      if (level()==0) tmp_string += "  ";
      else if (level()==1) tmp_string += " ";

      //tmp_string += "             "; // 13 spaces
      if (err_num<=9)
        tmp_string += "  ERROR #0"+std::to_string(err_num)+"  ";
      else
        tmp_string += "  ERROR #"+std::to_string(err_num)+"  ";

      if (level()==0) tmp_string += "  ";
      else if (level()==1) tmp_string += " ";

      // right boundary for my self
      if (task_in_color() == task_size()-1) tmp_string += " |";
      else tmp_string += "  ";
      tmp_string += PrintRightBoundary(level());
      tmp_string += "\n";
    }
    else {
      tmp_string += PrintEmptyLine();
    }


    //FIXME: may still need this line if it feels better
    //// print empty line after
    //tmp_string += PrintEmptyLine();

    fprintf(foutput, "%s", tmp_string.c_str());
    fflush(foutput);
  }
}

void CDHandle::PrintRecovery(FILE* foutput, int inject_rank){
  if (GetCDExecuteMode()!=kReexecution) return;
  std::string tmp_string="";

  if (inject_rank == -1){
    //tmp_string += PrintEmptyLine();
    tmp_string += PrintEmptyLine();
  }

  // print recover information
  tmp_string += PrintLeftBoundary(level());
  // left boundary for myself
  if (task_in_color()==0) tmp_string += "| ";
  else tmp_string += "  ";
  if (level()==0) tmp_string += "  ";
  else if (level()==1) tmp_string += " ";
  // 13 characters
  tmp_string += "L"+std::to_string(level())+" CD Recover";
  if (level()==0) tmp_string += "  ";
  else if (level()==1) tmp_string += " ";
  // right boundary for my self
  if (task_in_color() == task_size()-1) tmp_string += " |";
  else tmp_string += "  ";
  tmp_string += PrintRightBoundary(level());
  tmp_string += "\n";

  fprintf(foutput, "%s", tmp_string.c_str());
  fflush(foutput);
}

CDExecMode CDHandle::GetCDExecuteMode(void){
  return ptr_cd()->GetCDExecuteMode();
}

namespace internal{
//FIXME: to be implemented
uint32_t CD::SetRollbackPoint(const uint32_t &rollback_lv, bool remote) 
{
  return 0;
}

} // namespace internal

} // namespace cd

#endif

