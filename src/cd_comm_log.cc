/*
Copyright 2014, The University of Texas at Austin 
All rights reserved.

THIS FILE IS PART OF THE CONTAINMENT DOMAINS RUNTIME LIBRARY

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met: 

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer. 

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution. 

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/
#include "cd_config.h"

#ifdef comm_log

#include "cd_comm_log.h"
#include "cd_internal.h"
#include <string.h>

#include "upcxx/upcxx_internal.h"

using namespace cd;
using namespace cd::logging;
using namespace cd::internal;

uint64_t cd::logging::tot_alloc_mem_=0;

CommLog::CommLog(CD* my_cd, 
                 CommLogMode comm_log_mode)
  :queue_size_unit_(1024*1024), table_size_unit_(1024), child_log_size_unit_(1024*1024)
{
  my_cd_ = my_cd;
  comm_log_mode_ = comm_log_mode;
  InitInternal();
}

CommLog::CommLog(CD* my_cd, 
                 CommLogMode comm_log_mode, 
                 uint64_t queue_size_unit, 
                 uint64_t table_size_unit)
  :child_log_size_unit_(1024*1024)
{
  my_cd_ = my_cd;
  comm_log_mode_ = comm_log_mode;
  queue_size_unit_ = queue_size_unit;
  table_size_unit_ = table_size_unit;
  InitInternal();
}


CommLog::CommLog(CD* my_cd, 
                 CommLogMode comm_log_mode, 
                 uint64_t queue_size_unit, 
                 uint64_t table_size_unit, 
                 uint64_t child_log_size_unit)
{
  my_cd_ = my_cd;
  comm_log_mode_ = comm_log_mode;
  queue_size_unit_ = queue_size_unit;
  table_size_unit_ = table_size_unit;
  child_log_size_unit_ = child_log_size_unit;
  InitInternal();
}


CommLog::~CommLog()
{
  //destroy all allocated memory
  //log_table_
  if (!log_table_.base_ptr_.empty()) 
  {
    //LOG_DEBUG("delete log_table_.base_ptr_.\n");
    log_table_.base_ptr_.clear();
  }

  //log_queue_
  if (log_queue_.base_ptr_ != NULL)
  {
    //LOG_DEBUG("delete log_queue_.base_ptr_ (%p)\n", log_queue_.base_ptr_);
    delete log_queue_.base_ptr_;
  }

  //child_log_
  if (child_log_.base_ptr_ != NULL)
  {
    //LOG_DEBUG("delete child_log_.base_ptr_ (%p)\n", child_log_.base_ptr_);
    delete child_log_.base_ptr_;
  }
  
  //LOG_DEBUG("Reach the end of CommLog destructor...\n");
}


void CommLog::InitInternal()
{
  new_log_generated_ = false;

  log_queue_.queue_size_ = 0;
  log_queue_.cur_pos_ = 0;
  log_queue_.base_ptr_ = NULL;

  log_table_.table_size_ = 0;
  log_table_.cur_pos_ = 0;
  log_table_.base_ptr_.clear();

  child_log_.size_ = 0;
  child_log_.cur_pos_ = 0;
  child_log_.base_ptr_ = NULL;

  log_table_reexec_pos_ = 0;

  new_log_generated_ = false;

  // if forward execution
  if (comm_log_mode_ == kGenerateLog)
  {
    // allocate space for log table and log queue
    CommLogErrT ret = InitAlloc();
    if (ret == kCommLogInitFailed || ret == kCommLogError)
    {
      ERROR_MESSAGE("Communication Logs Initialization Failed!\n");
    }
  }
  //// in kReplayLog mode, no allocation because unpack will allocate data...
  //else if (comm_log_mode_ == kReplayLog)
  //{
  //}

}


// ReInit is called when a CD wants to reexecute
void CommLog::ReInit()
{
  comm_log_mode_ = kReplayLog;
  log_table_reexec_pos_ = 0;
}


CommLogErrT CommLog::InitAlloc()
{
  //TODO: should check if Init is called more than once??
  if (log_queue_.base_ptr_ == NULL)
  {
    cd::logging::tot_alloc_mem_ += queue_size_unit_;
    log_queue_.base_ptr_ = new char [queue_size_unit_];
    if (log_queue_.base_ptr_ == NULL) 
    {
      return kCommLogInitFailed;
    }
    log_queue_.queue_size_ = queue_size_unit_;
  }

  if (log_table_.base_ptr_.empty())
  {
    cd::logging::tot_alloc_mem_ += table_size_unit_;
    log_table_.base_ptr_.resize(table_size_unit_);
    if (log_table_.base_ptr_.empty()) 
    {
      return kCommLogInitFailed;
    }
    log_table_.table_size_ = table_size_unit_;
  }

  return kCommLogOK;
}


CommLogErrT CommLog::Realloc()
{
  log_queue_.queue_size_ = 0;
  log_queue_.cur_pos_ = 0;
  log_queue_.base_ptr_ = NULL;

  log_table_.table_size_ = 0;
  log_table_.cur_pos_ = 0;
  log_table_.base_ptr_.clear();

  child_log_.size_ = 0;
  child_log_.cur_pos_ = 0;
  child_log_.base_ptr_ = NULL;

  log_table_reexec_pos_ = 0;

  new_log_generated_ = false;

  CommLogErrT ret = InitAlloc();
  if (ret != kCommLogOK)
  {
    ERROR_MESSAGE("Comm Log reallocation failed!\n");
  }

  return kCommLogOK;
}


bool CommLog::ProbeAndLogData(void *addr, 
                              uint64_t length,
                              void *flag,
                              bool isrecv)
{
  uint64_t pos;
  bool found = false;
  if(isrecv) {
    // ??
  }
  // change entry incompleted entry to completed if there is any
  // and associate it with the data
  for (pos=0; pos<log_table_.cur_pos_; pos++)
  {
    if (log_table_.base_ptr_[pos].flag_ == flag)
    {
      // change log table
      // length stored is length from isend/irecv, so length should match here...
      LOG_DEBUG("log_table_.base_ptr_[%d].flag_=%p, and flag=%p\n", pos, log_table_.base_ptr_[pos].flag_, flag);
      assert(log_table_.base_ptr_[pos].length_ == length);
      log_table_.base_ptr_[pos].completed_ = true;

      // change log queue
      if (length!=0)
      {
        // copy data into the place that reserved..
        memcpy(log_queue_.base_ptr_+log_table_.base_ptr_[pos].pos_, addr, length);
      }

      found = true;
      break;
    }
  }

  return found;
}


// fill in all missing data and log data
void CommLog::ProbeAndLogDataImplicit()
{
  // 1) deal with logs in current CD
  unsigned long pos;
  // change entry incompleted entry to completed if there is any
  // and associate it with the data
  for (pos=0; pos<log_table_.cur_pos_; pos++){
    // should not copy data again for completed log entries..
    if (log_table_.base_ptr_[pos].completed_ == false){
      // change log table
      log_table_.base_ptr_[pos].completed_ = true;
      // change log queue
      if (log_table_.base_ptr_[pos].length_!=0){
        LOG_DEBUG("Copy implicit data from %p and length (%ld)...\n", log_table_.base_ptr_[pos].addr_, log_table_.base_ptr_[pos].length_);
        // copy data into the place that reserved..
        memcpy(log_queue_.base_ptr_+log_table_.base_ptr_[pos].pos_, 
              log_table_.base_ptr_[pos].addr_, 
              log_table_.base_ptr_[pos].length_);
      }
    }
  }

  // 2) deal with pushed logs from lower level CDs
  unsigned long inner_index=0, tmp_index=0, index=0;
  unsigned long tmp_length=0;
  struct LogTable tmp_log_table;
  struct LogTableElement * tmp_table_ptr;
  char * tmp_queue_ptr;
  struct LogQueue tmp_log_queue;
  char * dest_addr;

  while (index < child_log_.cur_pos_)
  {
    inner_index = index;
    memcpy(&tmp_length, child_log_.base_ptr_+inner_index, sizeof(uint64_t));
    inner_index += sizeof(uint64_t);
    inner_index += sizeof(CDID);
    memcpy(&tmp_log_table.cur_pos_, child_log_.base_ptr_+inner_index, sizeof(uint64_t));
    inner_index += sizeof(uint64_t);
    memcpy(&tmp_log_table.table_size_, child_log_.base_ptr_+inner_index, sizeof(uint64_t));
    inner_index += sizeof(uint64_t);
    tmp_index = 0;
    tmp_table_ptr = (struct LogTableElement *) (child_log_.base_ptr_+inner_index);
    // skip log table data
    inner_index += sizeof(struct LogTableElement)*tmp_log_table.cur_pos_;
    // copy out log queue metadata
    memcpy(&tmp_log_queue, child_log_.base_ptr_+inner_index, sizeof(struct LogQueue));
    // skip log queue metadata 
    inner_index += sizeof(struct LogQueue);
    tmp_queue_ptr = (char *) (child_log_.base_ptr_+inner_index);

    while (tmp_index < tmp_log_table.cur_pos_)
    {
      if (tmp_table_ptr->completed_ == false){
        LOG_DEBUG("Copy implicit data (total %ld log entries) for some child CD logs...\n", tmp_log_table.cur_pos_);
        LOG_DEBUG("child_log_.base_ptr_=%p, inner_index=%ld, child_log_.cur_pos_=%ld\n", child_log_.base_ptr_, inner_index, child_log_.cur_pos_);
        dest_addr = tmp_queue_ptr+tmp_table_ptr->pos_;
        LOG_DEBUG("tmp_queue_ptr=%p, and tmp_table_ptr->pos_=%ld\n", tmp_queue_ptr, tmp_table_ptr->pos_);
        LOG_DEBUG("tmp_log_queue.base_ptr_=%p, and tmp_log_queue.cur_pos_=%ld\n", tmp_log_queue.base_ptr_, tmp_log_queue.cur_pos_);
        // copy data in
        LOG_DEBUG("dest_addr=%p, tmp_table_ptr->addr_=%p, and length=%ld\n", dest_addr, tmp_table_ptr->addr_, sizeof(char)*tmp_table_ptr->length_);
        memcpy(dest_addr, tmp_table_ptr->addr_, sizeof(char)*tmp_table_ptr->length_);
        // change incomplete status to complete
        tmp_table_ptr->completed_ = true;
      }

      tmp_index++;
      tmp_table_ptr++;
    }

    // skip log queue data
    inner_index += sizeof(char)*tmp_log_queue.cur_pos_;
    // skip child log queue metadata
    inner_index += sizeof(struct ChildLogQueue); 
    
    // fast forward index
    index = inner_index;
  }

  // this assert should pass if index calculation is correct
  //LOG_DEBUG("Before assertion: index=%ld, and child_log_.cur_pos_=%ld\n", index, child_log_.cur_pos_);
  assert(index == child_log_.cur_pos_);
}


bool CommLog::ProbeAndLogDataPacked(void *addr, 
                                    uint64_t length,
                                    void *flag,
                                    bool isrecv)
{
  // 1) find the corresponding log entry in child_log_
  // 2) assert length 
  // 3) find corresponding log queue address and copy data in
  // 4) change incomplete flag to complete

  bool found = false;
  uint64_t inner_index=0, tmp_index, index=0;
  uint64_t tmp_length=0;
  struct LogTable tmp_log_table;
  struct LogTableElement *tmp_table_ptr;
  struct LogQueue tmp_log_queue;
  char *dest_addr;

  while (index < child_log_.cur_pos_)
  {
    inner_index = index;
    memcpy(&tmp_length, child_log_.base_ptr_+inner_index, sizeof(uint64_t));
    inner_index += sizeof(uint64_t);
    inner_index += sizeof(CDID);
    memcpy(&tmp_log_table.cur_pos_, child_log_.base_ptr_+inner_index, sizeof(uint64_t));
    inner_index += sizeof(uint64_t);
    memcpy(&tmp_log_table.table_size_, child_log_.base_ptr_+inner_index, sizeof(uint64_t));
    inner_index += sizeof(uint64_t);
    tmp_index = 0;
    tmp_table_ptr = (struct LogTableElement *) (child_log_.base_ptr_+inner_index);
    while (tmp_index < tmp_log_table.cur_pos_)
    {
      //LOG_DEBUG("tmp_table_ptr->flag_=%p, and flag=%p\n", tmp_table_ptr->flag_, flag);
      if (tmp_table_ptr->flag_ == flag && tmp_table_ptr->completed_==false){
        // if found the corresponding log entry
        assert(tmp_table_ptr->length_ == length);
        found = true;
        break;
      }

      tmp_index++;
      tmp_table_ptr++;
    }

    if (found)
    {
      // find corresponding queue space to store data
      inner_index += sizeof(struct LogTableElement)*tmp_log_table.cur_pos_;
      inner_index += sizeof(struct LogQueue);
      dest_addr = (char*)(child_log_.base_ptr_+inner_index+tmp_table_ptr->pos_);

      // copy data in
      memcpy(dest_addr, addr, sizeof(char)*length);

      // change incomplete status to complete
      tmp_table_ptr->completed_ = true;
    
      break;
    }

    // jump to next packed log
    // skip log table data
    inner_index += sizeof(struct LogTableElement)*tmp_log_table.cur_pos_;
    // copy out log queue metadata
    memcpy(&tmp_log_queue, child_log_.base_ptr_+inner_index, sizeof(struct LogQueue));
    // skip log queue metadata 
    inner_index += sizeof(struct LogQueue);
    // skip log queue data
    inner_index += sizeof(char)*tmp_log_queue.cur_pos_;
    // skip child log queue metadata
    inner_index += sizeof(struct ChildLogQueue); 
    
    // fast forward index
    index = inner_index;
    //LOG_DEBUG("index reaches %ld\n", index);
  }
  // this assert should pass if index calculation is correct
  //LOG_DEBUG("before assertion: found=%d, index=%ld, and child_log_.cur_pos_=%ld\n", found, index, child_log_.cur_pos_);
  assert(found || (index == child_log_.cur_pos_));

  return found;
}


void CommLog::ProbeAndLogDataPackedImplicit()
{
  unsigned long inner_index=0, tmp_index, index=0;
  unsigned long tmp_length=0;
  struct LogTable tmp_log_table;
  struct LogTableElement * tmp_table_ptr;
  char * tmp_queue_ptr;
  struct LogQueue tmp_log_queue;
  char * dest_addr;

  while (index < child_log_.cur_pos_)
  {
    inner_index = index;
    memcpy(&tmp_length, child_log_.base_ptr_+inner_index, sizeof(uint64_t));
    inner_index += sizeof(uint64_t);
    inner_index += sizeof(CDID);
    memcpy(&tmp_log_table.cur_pos_, child_log_.base_ptr_+inner_index, sizeof(uint64_t));
    inner_index += sizeof(uint64_t);
    memcpy(&tmp_log_table.table_size_, child_log_.base_ptr_+inner_index, sizeof(uint64_t));
    inner_index += sizeof(uint64_t);
    tmp_index = 0;
    tmp_table_ptr = (struct LogTableElement *) (child_log_.base_ptr_+inner_index);
    // skip log table data
    inner_index += sizeof(struct LogTableElement)*tmp_log_table.cur_pos_;
    // copy out log queue metadata
    memcpy(&tmp_log_queue, child_log_.base_ptr_+inner_index, sizeof(struct LogQueue));
    // skip log queue metadata 
    inner_index += sizeof(struct LogQueue);
    tmp_queue_ptr = (char *) (child_log_.base_ptr_+inner_index);

    while (tmp_index < tmp_log_table.cur_pos_)
    {
      dest_addr = tmp_queue_ptr+tmp_table_ptr->pos_;
      // copy data in
      memcpy(dest_addr, tmp_table_ptr->addr_, sizeof(char)*tmp_table_ptr->length_);
      // change incomplete status to complete
      tmp_table_ptr->completed_ = true;

      tmp_index++;
      tmp_table_ptr++;
    }

    // skip child log queue metadata
    inner_index += sizeof(struct ChildLogQueue); 
    
    // fast forward index
    index = inner_index;
  }

  // this assert should pass if index calculation is correct
  //LOG_DEBUG("before assertion: index=%ld, and child_log_.cur_pos_=%ld\n", index, child_log_.cur_pos_);
  assert(index == child_log_.cur_pos_);

}

bool CommLog::FoundRepeatedEntry(const void *data_ptr, uint64_t data_length, 
                                 bool completed, void *flag)
{
  //LOG_DEBUG("Inside FoundRepeatedEntry:\n");
  //LOG_DEBUG("isrepeated_=%d\n", log_table_.base_ptr_[log_table_.cur_pos_-1].isrepeated_);
  //LOG_DEBUG("length_=%ld vs data_length=%ld\n", log_table_.base_ptr_[log_table_.cur_pos_-1].length_, data_length);
  //LOG_DEBUG("completed_=%d vs completed=%d\n", log_table_.base_ptr_[log_table_.cur_pos_-1].completed_, completed);
  //LOG_DEBUG("flag_=%ld vs flag=%ld\n", log_table_.base_ptr_[log_table_.cur_pos_-1].flag_, flag);

  return log_table_.base_ptr_[log_table_.cur_pos_-1].isrepeated_ &&
      log_table_.base_ptr_[log_table_.cur_pos_-1].length_ == data_length &&
      log_table_.base_ptr_[log_table_.cur_pos_-1].completed_ == completed &&
      log_table_.base_ptr_[log_table_.cur_pos_-1].flag_ == flag;

}

#if CD_PGAS_ENABLED
CommLogErrT CommLog::LogWriteData(const void *tgt_addr, unsigned long data_length, uint32_t taskID, 
                                  uint32_t sc, uint32_t iwc, void* flag/*src_addr*/)
{
  CommLogErrT ret;

  LOG_DEBUG("LogWriteData of tgt address (%p), length(%ld), and taskID(%d), flag(%p)\n",tgt_addr,data_length,taskID,flag);

  ret = WriteLogTableWithCounters(taskID, tgt_addr, data_length, sc, iwc, flag);
  if (ret == kCommLogAllocFailed) 
  {
    ERROR_MESSAGE("Log Table Realloc Failed!\n");
    return ret;
  }

  // write data into the queue
  if (data_length!=0)
  {
    ret = WriteLogQueue(flag/*src_addr*/, data_length, true);
    if (ret == kCommLogAllocFailed) 
    {
      ERROR_MESSAGE("Log Queue Realloc Failed!\n");
      return ret;
    }
  }

  new_log_generated_ = true;
  
  return kCommLogOK;
}

CommLogErrT CommLog::PackWriteLogs(char** buffer, size_t* plen){
  // combine data entries with their corresponding counter entries
  // wait for put_reply if some counters missing.
  CombineWriteDataWithCounters();

  // calculate length of data copy
  // pack format is [SIZE][CDID][Table][Queue][ChildLogQueue]
  uint64_t length;
  length = sizeof(uint64_t) + sizeof(CDID) // SIZE + CDID
         + sizeof(uint64_t) + sizeof(uint64_t) + sizeof(struct LogTableElement)*log_table_.cur_pos_ //Table
         + sizeof(struct LogQueue) + sizeof(char)*log_queue_.cur_pos_ // Queue
         + sizeof(struct ChildLogQueue) + sizeof(char)*child_log_.cur_pos_; // ChildLogQueue

  *plen = length;

  cd::logging::tot_alloc_mem_ += length;

  //*buffer = (char *)malloc(length);
  *buffer = (char*)upcxx::gasnet_seg_alloc(length);
  if (*buffer == NULL) return kCommLogAllocFailed;

  // pack write logs for network tranportation..
  PackWriteLogInner(*buffer, length);

  return kCommLogOK;
}

CommLogErrT CommLog::PackWriteLogInner(char* buffer, uint64_t length)
{
  LOG_DEBUG("buffer=%p\n", buffer);

  uint64_t size;
  uint64_t cur_pos=0;

  //[SIZE]
  size = sizeof(uint64_t);
  memcpy (&(buffer[cur_pos]), &length, size);
  cur_pos += size;

  //[CDID]
  size = sizeof(CDID);
  CDID tmp_cd_id = my_cd_->GetCDID();
  memcpy (&(buffer[cur_pos]), &(tmp_cd_id), size);
  cur_pos += size;

  //[Table] meta data -- cur_pos_ and table_size_
  size = sizeof(uint64_t);
  memcpy (&(buffer[cur_pos]), &log_table_.cur_pos_, size);
  cur_pos += size;
  memcpy (&(buffer[cur_pos]), &log_table_.table_size_, size);
  cur_pos += size;

  //[Table] data
  size = sizeof(struct LogTableElement)*log_table_.cur_pos_;
  memcpy (&(buffer[cur_pos]), &log_table_.base_ptr_[0], size);
  cur_pos += size;

  //[Queue] meta data
  size = sizeof(struct LogQueue);
  memcpy (&(buffer[cur_pos]), &log_queue_, size);
  cur_pos += size;

  //[Queue] data
  size = sizeof(char)*log_queue_.cur_pos_;
  memcpy (&(buffer[cur_pos]), log_queue_.base_ptr_, size);
  cur_pos += size;

  //[ChildLogQueue] meta data
  size = sizeof(struct ChildLogQueue);
  memcpy (&(buffer[cur_pos]), &child_log_, size);
  cur_pos += size;

  //[ChildLogQueue] data
  size = sizeof(char)*child_log_.cur_pos_;
  memcpy (&(buffer[cur_pos]), child_log_.base_ptr_, size);
  cur_pos += size;

  assert(cur_pos == length);

  return kCommLogOK;
}

CommLogErrT CommLog::UnpackWriteLogs(char* buffer, size_t length, size_t dst_rank)
{
  // unpack write logs
  LOG_DEBUG("Unpack %lu bytes write logs from rank %lu..\n", length, dst_rank);
  UnpackLogs(buffer, length);

#if CD_DEBUG_ENABLED
  LOG_DEBUG("Print write logs after unpacking:\n");
  PrintWriteLogTable();
#endif

  return kCommLogOK;
}

CommLogErrT CommLog::ReplayWriteLogs(uint32_t sc)
{
  for (int ii=0; ii<log_table_.cur_pos_; ii++){
    if (log_table_.base_ptr_[ii].islive_==true 
          && log_table_.base_ptr_[ii].sync_counter_ == sc
          && log_table_.base_ptr_[ii].length_!=0) {

      LOG_DEBUG("Replay write logs: addr_=%p, length_=%u, sc_=0x%x, iwc=0x%x\n", 
                  log_table_.base_ptr_[ii].addr_, 
                  log_table_.base_ptr_[ii].length_, 
                  log_table_.base_ptr_[ii].sync_counter_, 
                  log_table_.base_ptr_[ii].incoming_write_counter_);

      memcpy(log_table_.base_ptr_[ii].addr_, 
             log_queue_.base_ptr_+log_table_.base_ptr_[ii].pos_,
             log_table_.base_ptr_[ii].length_);
    }
  }
  return kCommLogOK;
}

CommLogErrT CommLog::ReplayWriteLogsWithInvalidCounters(std::vector<IncompleteLogEntry> & all_invalid_counter_entries)
{
  std::unordered_set<void*> all_addrs;
  std::vector<IncompleteLogEntry> local_entries;
  for (int ii=0; ii<log_table_.cur_pos_; ii++){
    if (log_table_.base_ptr_[ii].islive_ == true
          && log_table_.base_ptr_[ii].sync_counter_ == INVALID_COUNTER
          && log_table_.base_ptr_[ii].length_ != 0) {

      LOG_DEBUG("Replay invalid-counter write logs: addr_=%p, length_=%u, sc_=0x%x, iwc=0x%x\n", 
                  log_table_.base_ptr_[ii].addr_, 
                  log_table_.base_ptr_[ii].length_, 
                  log_table_.base_ptr_[ii].sync_counter_, 
                  log_table_.base_ptr_[ii].incoming_write_counter_);

      memcpy(log_table_.base_ptr_[ii].addr_, 
             log_queue_.base_ptr_+log_table_.base_ptr_[ii].pos_,
             log_table_.base_ptr_[ii].length_);

      std::unordered_set<void*>::iterator aaii = all_addrs.find(log_table_.base_ptr_[ii].addr_);
      if (aaii != all_addrs.end()){
        WARNING_MESSAGE("thread %u accesses same address %p more than one time.\n", 
                    my_cd_->GetGlobalTaskInColor(), log_table_.base_ptr_[ii].addr_);

        // modify length_ information if this length_ is larger than last one
        int found=0;
        for (std::vector<IncompleteLogEntry>::iterator jj=local_entries.begin(); jj!=local_entries.end(); jj++) {
          if ((*jj).addr_==log_table_.base_ptr_[ii].addr_){
            if ((*jj).length_<log_table_.base_ptr_[ii].length_){
              (*jj).length_ = log_table_.base_ptr_[ii].length_;
            }
            found=1;
            break;
          }
        }
        assert(found==1);
      }
      else {
        all_addrs.insert(log_table_.base_ptr_[ii].addr_);

        IncompleteLogEntry tmp;
        tmp.taskID_ = my_cd_->GetGlobalTaskInColor();
        tmp.addr_ = log_table_.base_ptr_[ii].addr_;
        tmp.length_ = log_table_.base_ptr_[ii].length_;
        local_entries.push_back(tmp);
      }
    }
  }
  if (!local_entries.empty()){
    all_invalid_counter_entries.insert(all_invalid_counter_entries.end(), local_entries.begin(), local_entries.end());
  }
  return kCommLogOK;
}

CommLogErrT CommLog::WriteLogTableWithCounters (uint32_t taskID, 
                                                const void *tgt_addr, 
                                                uint64_t data_length, 
                                                uint32_t sc, 
                                                uint32_t iwc,
                                                void *flag/*src_addr*/)
{
  CommLogErrT ret;
  if (log_table_.cur_pos_ >= log_table_.table_size_) 
  {
    ret = IncreaseLogTableSize();
    if (ret == kCommLogAllocFailed) return ret;
  }

  log_table_.base_ptr_[log_table_.cur_pos_].pos_ = log_queue_.cur_pos_;
  log_table_.base_ptr_[log_table_.cur_pos_].length_ = data_length;
  log_table_.base_ptr_[log_table_.cur_pos_].completed_ = true;
  log_table_.base_ptr_[log_table_.cur_pos_].flag_ = flag;
  log_table_.base_ptr_[log_table_.cur_pos_].counter_ = 1;
  log_table_.base_ptr_[log_table_.cur_pos_].reexec_counter_ = 0;
  log_table_.base_ptr_[log_table_.cur_pos_].isrepeated_ = false;
  log_table_.base_ptr_[log_table_.cur_pos_].taskID_ = taskID;
  log_table_.base_ptr_[log_table_.cur_pos_].addr_ = (void*)tgt_addr;
  log_table_.base_ptr_[log_table_.cur_pos_].sync_counter_ = sc;
  log_table_.base_ptr_[log_table_.cur_pos_].incoming_write_counter_ = iwc;
  log_table_.cur_pos_++;

  return kCommLogOK;
}

void CommLog::CombineWriteDataWithCounters(int* start, int task_id)
{
  int ii;
  if (start==NULL) ii=0;
  else ii=*start;

#if CD_DEBUG_ENABLED
  //LOG_DEBUG("print write log table before combination..\n");
  //PrintWriteLogTable();
#endif
  for (; ii<log_table_.cur_pos_; ii++){
    if (log_table_.base_ptr_[ii].length_!=0 &&
          (log_table_.base_ptr_[ii].sync_counter_==INVALID_COUNTER || 
           log_table_.base_ptr_[ii].incoming_write_counter_==INVALID_COUNTER)) //found one write data entry
    {
      void* taddr = log_table_.base_ptr_[ii].addr_;
      void* tflag = log_table_.base_ptr_[ii].flag_;
      int jj;
      for(jj=ii+1; jj<log_table_.cur_pos_; jj++){
        if (log_table_.base_ptr_[jj].length_==0 
            && log_table_.base_ptr_[jj].islive_ == true
            && log_table_.base_ptr_[jj].addr_ == taddr
            && log_table_.base_ptr_[jj].flag_ == tflag) //found corresponding counters
        {
          // fix sc/iwc values
          log_table_.base_ptr_[ii].sync_counter_
            = log_table_.base_ptr_[jj].sync_counter_;
          log_table_.base_ptr_[ii].incoming_write_counter_
            = log_table_.base_ptr_[jj].incoming_write_counter_;

          log_table_.base_ptr_[jj].islive_ = false;
          break;
        }
      }

      if (jj==log_table_.cur_pos_){
      #if CD_DEBUG_ENABLED
        //WARNING_MESSAGE("counters info not present for (%d)...\n", ii);
        //PrintWriteLogTableSTD();
        PrintWriteLogTable();
      #endif
        if (start!=NULL) *start=ii;

        if (task_id!=-1){
          OutstandingWriteInfo tmp(task_id, log_table_.base_ptr_[ii].addr_);
          my_cd_->outstanding_writes_store_.push_back(tmp);
          printf("outstanding_writes_store_.size()=%d:\n", my_cd_->outstanding_writes_store_.size());
          int tindex=0;
          for(OutstandingWritesStore::iterator kk=my_cd_->outstanding_writes_store_.begin(); 
                  kk!=my_cd_->outstanding_writes_store_.end(); kk++) {
            printf("[%d]:%d,%p\n", tindex++, kk->target_id_, kk->addr_);
          }
        }
        fflush(stdout);
      }
      ///else { // successfully change sc/iwc for entry ii
      ///  ////FIXME: why need to change this addr to flag????
      ///  // change addr_ to tgt_addr
      ///  log_table_.base_ptr_[ii].addr_
      ///    = log_table_.base_ptr_[ii].flag_;
      ///}
    }
  }
}

void CommLog::PrintWriteLogTable()
{
  LOG_DEBUG("log_table_:\n");
  LOG_DEBUG("base_ptr_ = %p\n", &log_table_.base_ptr_[0]);
  LOG_DEBUG("cur_pos_ = %ld\n", log_table_.cur_pos_);
  LOG_DEBUG("table_size_ = %ld\n", log_table_.table_size_);
  
  uint64_t ii;
  for (ii=0;ii<log_table_.cur_pos_;ii++)
  {
    LOG_DEBUG("log_table_.base_ptr_[%ld]:\n", ii);
    LOG_DEBUG("pos_=%ld, length_=%ld, addr_=%p, sc_=0x%x, iwc_=0x%x, flag_=%p, islive_=%d\n"
        ,log_table_.base_ptr_[ii].pos_
        ,log_table_.base_ptr_[ii].length_
        ,log_table_.base_ptr_[ii].addr_
        ,log_table_.base_ptr_[ii].sync_counter_
        ,log_table_.base_ptr_[ii].incoming_write_counter_
        ,log_table_.base_ptr_[ii].flag_
        ,log_table_.base_ptr_[ii].islive_);
  }
}

void CommLog::PrintWriteLogTableSTD()
{
  printf("log_table_:\n");
  printf("base_ptr_ = %p\n", &log_table_.base_ptr_[0]);
  printf("cur_pos_ = %ld\n", log_table_.cur_pos_);
  printf("table_size_ = %ld\n", log_table_.table_size_);
  
  uint64_t ii;
  for (ii=0;ii<log_table_.cur_pos_;ii++)
  {
    printf("log_table_.base_ptr_[%ld]:\n", ii);
    printf("pos_=%ld, length_=%ld, addr_=%p, sc_=0x%x, iwc_=0x%x, islive_=%d\n"
        ,log_table_.base_ptr_[ii].pos_
        ,log_table_.base_ptr_[ii].length_
        ,log_table_.base_ptr_[ii].addr_
        ,log_table_.base_ptr_[ii].sync_counter_
        ,log_table_.base_ptr_[ii].incoming_write_counter_
        ,log_table_.base_ptr_[ii].islive_);
  }
}

#endif

CommLogErrT CommLog::LogData(const void *data_ptr, uint64_t data_length, uint32_t taskID,
                          bool completed, void *flag, 
                          bool isrecv, bool isrepeated, 
                          bool intra_cd_msg, int tag, ColorT comm)
{
  CommLogErrT ret;

  if (isrepeated)
  {
    // check if repeated log entry exists
    // if exists, just increment the counter_ and return
    if (FoundRepeatedEntry(data_ptr, data_length, completed, flag))
    {
      //LOG_DEBUG("Found current repeating log entry matching!\n");
      log_table_.base_ptr_[log_table_.cur_pos_-1].counter_++;
      return kCommLogOK;
    }
  }

  LOG_DEBUG("LogData of address (%p) and length(%ld) on entry #%ld\n", data_ptr, data_length, log_table_.cur_pos_);

  ret = WriteLogTable(taskID, data_ptr, data_length, completed, flag, isrepeated);
  if (ret == kCommLogAllocFailed) 
  {
    ERROR_MESSAGE("Log Table Realloc Failed!\n");
    return ret;
  }

  // write data into the queue
  if (data_length!=0)
  {
    ret = WriteLogQueue(data_ptr, data_length, completed);
    if (ret == kCommLogAllocFailed) 
    {
      ERROR_MESSAGE("Log Queue Realloc Failed!\n");
      return ret;
    }
  }

  // insert one incomplete_log_entry_ to incomplete_log_
  if (!completed)
  {
    // append one entry at the end of my_cd_->incomplete_log_
    IncompleteLogEntry tmp_log_entry;
    tmp_log_entry.addr_ = const_cast<void *>(data_ptr);
    tmp_log_entry.length_ = (uint64_t) data_length;
    tmp_log_entry.flag_ =  (uint64_t)flag;
    tmp_log_entry.complete_ = false;
    tmp_log_entry.isrecv_ = isrecv;
    tmp_log_entry.taskID_ = taskID;
    tmp_log_entry.intra_cd_msg_ = intra_cd_msg;
    tmp_log_entry.tag_ = tag;
    #if _MPI_VER
    tmp_log_entry.comm_ = comm;
    #endif

#ifdef libc_log
    //GONG
    tmp_log_entry.p_ = NULL;
#endif

    my_cd_->incomplete_log_.push_back(tmp_log_entry);
#if _DEBUG
    my_cd_->PrintIncompleteLog();
#endif
  }

  new_log_generated_ = true;

  return kCommLogOK;
}


CommLogErrT CommLog::WriteLogTable (uint32_t taskID, const void *data_ptr, uint64_t data_length, 
                                  bool completed, void *flag, bool isrepeated)
{
  CommLogErrT ret;
  if (log_table_.cur_pos_ >= log_table_.table_size_) 
  {
    ret = IncreaseLogTableSize();
    if (ret == kCommLogAllocFailed) return ret;
  }

  if(data_ptr == NULL) {
    //??
  }

  log_table_.base_ptr_[log_table_.cur_pos_].pos_ = log_queue_.cur_pos_;
  log_table_.base_ptr_[log_table_.cur_pos_].length_ = data_length;
  log_table_.base_ptr_[log_table_.cur_pos_].completed_ = completed;
  log_table_.base_ptr_[log_table_.cur_pos_].flag_ = flag;
  log_table_.base_ptr_[log_table_.cur_pos_].counter_ = 1;
  log_table_.base_ptr_[log_table_.cur_pos_].reexec_counter_ = 0;
  log_table_.base_ptr_[log_table_.cur_pos_].isrepeated_ = isrepeated;
  log_table_.base_ptr_[log_table_.cur_pos_].taskID_ = taskID;
  log_table_.base_ptr_[log_table_.cur_pos_].addr_ = (void*)data_ptr;
  log_table_.cur_pos_++;

  return kCommLogOK;
}


CommLogErrT CommLog::WriteLogQueue (const void *data_ptr, uint64_t data_length, bool completed)
{
  CommLogErrT ret;
  if (log_queue_.cur_pos_ + data_length > log_queue_.queue_size_)
  {
    ret = IncreaseLogQueueSize(data_length);
    if (ret == kCommLogAllocFailed) return ret;
  }

  // TODO: check memcpy success??
  if (completed && data_length!=0)
  {
    memcpy(log_queue_.base_ptr_+log_queue_.cur_pos_, data_ptr, data_length);
  }
  log_queue_.cur_pos_ += data_length;

  return kCommLogOK;
}


CommLogErrT CommLog::IncreaseLogTableSize()
{
  //struct LogTableElement *tmp_ptr 
  //  = new struct LogTableElement [log_table_.table_size_ + table_size_unit_];
  //if (tmp_ptr == NULL) return kCommLogAllocFailed;

  // copy old data in
  //memcpy (tmp_ptr, log_table_.base_ptr_, log_table_.table_size_ *sizeof(struct LogTableElement));
  cd::logging::tot_alloc_mem_ += table_size_unit_;
  log_table_.base_ptr_.resize(log_table_.table_size_ + table_size_unit_);

  //// free log_table_.base_ptr_
  //delete log_table_.base_ptr_;
  //// change log_table_.base_ptr_ to tmp_ptr
  //log_table_.base_ptr_ = tmp_ptr;

  // increase table_size_
  log_table_.table_size_ += table_size_unit_;

  return kCommLogOK;
}


// increase log queue size to hold data of size of "length"
CommLogErrT CommLog::IncreaseLogQueueSize(uint64_t length)
{
  uint64_t required_length = ((log_queue_.cur_pos_+length)/queue_size_unit_+1)*queue_size_unit_;
  if (required_length <= log_queue_.queue_size_)
  {
    ERROR_MESSAGE("Inside IncreaseLogQueueSize, required_length (%ld) is smaller than log_queue_.queue_size_ (%ld)!!\n",
                  required_length, log_queue_.queue_size_);
    return kCommLogError;
  }
  cd::logging::tot_alloc_mem_ += required_length;
  char *tmp_ptr = new char [required_length];
  if (tmp_ptr == NULL) return kCommLogAllocFailed;

  // copy old data in
  memcpy (tmp_ptr, log_queue_.base_ptr_, log_queue_.queue_size_ *sizeof(char));

  // free log_queue_.base_ptr_
  delete log_queue_.base_ptr_;

  // change log_queue_.base_ptr_ to tmp_ptr
  log_queue_.base_ptr_ = tmp_ptr;

  // increase queue_size_
  log_queue_.queue_size_ = required_length;

  return kCommLogOK;
}


CommLogErrT CommLog::CheckChildLogAlloc(uint64_t length)
{
  // if first time copy data into child_log_.base_ptr_
  if (child_log_.base_ptr_ == NULL) 
  {
    uint64_t required_length = (length/child_log_size_unit_ + 1)*child_log_size_unit_;
    child_log_.base_ptr_ = new char [required_length];
    cd::logging::tot_alloc_mem_ += required_length;
    if (child_log_.base_ptr_ == NULL) return kCommLogChildLogAllocFailed;
    child_log_.size_ = required_length;
    child_log_.cur_pos_ = 0;
  }
  // if not enough space in child_log_.base_ptr_
  else if (child_log_.cur_pos_ + length >= child_log_.size_)
  {
    uint64_t required_length = 
      ((child_log_.cur_pos_+length)/child_log_size_unit_ + 1)*child_log_size_unit_;

    char *tmp_ptr = new char [required_length];
    cd::logging::tot_alloc_mem_ += required_length;
    if (tmp_ptr == NULL) return kCommLogChildLogAllocFailed;
    memcpy (tmp_ptr, child_log_.base_ptr_, child_log_.cur_pos_ *sizeof(char));
    delete child_log_.base_ptr_;
    child_log_.base_ptr_ = tmp_ptr;
    child_log_.size_ = required_length;
  }

  return kCommLogOK;
}


//SZ: FIXME: what if parent_cd and cd are in different address space???
CommLogErrT CommLog::PackAndPushLogs(CD* parent_cd)
{
  // calculate length of data copy
  // pack format is [SIZE][CDID][Table][Queue][ChildLogQueue]
  uint64_t length;
  length = sizeof(uint64_t) + sizeof(CDID) // SIZE + CDID
         + sizeof(uint64_t) + sizeof(uint64_t) + sizeof(struct LogTableElement)*log_table_.cur_pos_ //Table
         + sizeof(struct LogQueue) + sizeof(char)*log_queue_.cur_pos_ // Queue
         + sizeof(struct ChildLogQueue) + sizeof(char)*child_log_.cur_pos_; // ChildLogQueue

  // check parent's queue availability
  parent_cd->CommLogCheckAlloc(length);

  // TODO: this is ugly, what about a tmp_ptr to pack and then copy to parent??
  // pack logs to parent's child_log_.base_ptr_
  PackLogs(parent_cd->comm_log_ptr_, length);

  return kCommLogOK;
}

CommLogErrT CommLog::PackAndPushImplicitLogs(CD* parent_cd)
{
  // calculate length of data copy
  // pack format is [SIZE][CDID][Table][Queue][ChildLogQueue]
  uint64_t length;
  length = sizeof(uint64_t) + sizeof(CDID) // SIZE + CDID
         + sizeof(uint64_t) + sizeof(uint64_t) + sizeof(struct LogTableElement)*log_table_.cur_pos_ //Table
         + sizeof(struct LogQueue) + sizeof(char)*log_queue_.cur_pos_ // Queue
         + sizeof(struct ChildLogQueue) + sizeof(char)*child_log_.cur_pos_; // ChildLogQueue

  // check parent's queue availability
  parent_cd->CommLogCheckAllocImplicit(length);

  // TODO: this is ugly, what about a tmp_ptr to pack and then copy to parent??
  // pack logs to parent's child_log_.base_ptr_
  PackLogs(parent_cd->implicit_comm_log_ptr_, length);

  return kCommLogOK;
}

#ifdef libc_log
//GONG
CommLogErrT CommLog::PackAndPushLogs_libc(CD* parent_cd)
{
  // calculate length of data copy
  // pack format is [SIZE][CDID][Table][Queue][ChildLogQueue]
  uint64_t length;
  length = sizeof(uint64_t) + sizeof(CDID) // SIZE + CDID
         + sizeof(uint64_t) + sizeof(uint64_t) + sizeof(struct LogTableElement)*log_table_.cur_pos_ //Table
         + sizeof(struct LogQueue) + sizeof(char)*log_queue_.cur_pos_ // Queue
         + sizeof(struct ChildLogQueue) + sizeof(char)*child_log_.cur_pos_; // ChildLogQueue

  // check parent's queue availability
  parent_cd->CommLogCheckAlloc_libc(length);

  // TODO: this is ugly, what about a tmp_ptr to pack and then copy to parent??
  // pack logs to parent's child_log_.base_ptr_
  PackLogs(parent_cd->libc_log_ptr_, length);

  return kCommLogOK;
}
#endif

CommLogErrT CommLog::PackLogs(CommLog *dst_cl_ptr, uint64_t length)
{
  if (dst_cl_ptr == NULL) return kCommLogError;

  LOG_DEBUG("dst_cl_ptr=%p\n", dst_cl_ptr);
  LOG_DEBUG("dst_cl_ptr->child_log_.base_ptr_=%p\n", dst_cl_ptr->child_log_.base_ptr_);
  //LOG_DEBUG("before: child_log_.size_= %ld\n", dst_cl_ptr->child_log_.size_);
  //LOG_DEBUG("before: child_log_.cur_pos_ = %ld\n", dst_cl_ptr->child_log_.cur_pos_);

  uint64_t size;
  //[SIZE]
  size = sizeof(uint64_t);
  memcpy (&(dst_cl_ptr->child_log_.base_ptr_[dst_cl_ptr->child_log_.cur_pos_]), 
      &length, size);
  dst_cl_ptr->child_log_.cur_pos_ += size;
  //LOG_DEBUG("After size: child_log_.cur_pos_ = %ld\n", dst_cl_ptr->child_log_.cur_pos_);

  //[CDID]
  size = sizeof(CDID);
  CDID tmp_cd_id = my_cd_->GetCDID();
  memcpy (&(dst_cl_ptr->child_log_.base_ptr_[dst_cl_ptr->child_log_.cur_pos_]), 
      &(tmp_cd_id), size);
  dst_cl_ptr->child_log_.cur_pos_ += size;
  //LOG_DEBUG("After CDID: child_log_.cur_pos_ = %ld\n", dst_cl_ptr->child_log_.cur_pos_);

  //[Table] meta data -- cur_pos_ and table_size_
  size = sizeof(uint64_t);
  memcpy (&(dst_cl_ptr->child_log_.base_ptr_[dst_cl_ptr->child_log_.cur_pos_]), 
      &log_table_.cur_pos_, size);
  dst_cl_ptr->child_log_.cur_pos_ += size;
  size = sizeof(uint64_t);
  memcpy (&(dst_cl_ptr->child_log_.base_ptr_[dst_cl_ptr->child_log_.cur_pos_]), 
      &log_table_.table_size_, size);
  dst_cl_ptr->child_log_.cur_pos_ += size;
  //LOG_DEBUG("After table meta data: child_log_.cur_pos_ = %ld\n", dst_cl_ptr->child_log_.cur_pos_);

  //[Table] data
  size = sizeof(struct LogTableElement)*log_table_.cur_pos_;
  memcpy (&(dst_cl_ptr->child_log_.base_ptr_[dst_cl_ptr->child_log_.cur_pos_]), 
      &log_table_.base_ptr_[0], size);
  dst_cl_ptr->child_log_.cur_pos_ += size;
  //LOG_DEBUG("After table data: child_log_.cur_pos_ = %ld\n", dst_cl_ptr->child_log_.cur_pos_);

  //[Queue] meta data
  size = sizeof(struct LogQueue);
  memcpy (&(dst_cl_ptr->child_log_.base_ptr_[dst_cl_ptr->child_log_.cur_pos_]), 
      &log_queue_, size);
  dst_cl_ptr->child_log_.cur_pos_ += size;
  //LOG_DEBUG("After queue meta data: child_log_.cur_pos_ = %ld\n", dst_cl_ptr->child_log_.cur_pos_);

  //[Queue] data
  size = sizeof(char)*log_queue_.cur_pos_;
  memcpy (&(dst_cl_ptr->child_log_.base_ptr_[dst_cl_ptr->child_log_.cur_pos_]), 
      log_queue_.base_ptr_, size);
  dst_cl_ptr->child_log_.cur_pos_ += size;
  //LOG_DEBUG("After queue data: child_log_.cur_pos_ = %ld\n", dst_cl_ptr->child_log_.cur_pos_);

  //[ChildLogQueue] meta data
  size = sizeof(struct ChildLogQueue);
  memcpy (&(dst_cl_ptr->child_log_.base_ptr_[dst_cl_ptr->child_log_.cur_pos_]), 
      &child_log_, size);
  dst_cl_ptr->child_log_.cur_pos_ += size;
  //LOG_DEBUG("After child queue meta data: child_log_.cur_pos_ = %ld\n", dst_cl_ptr->child_log_.cur_pos_);

  //[ChildLogQueue] data
  size = sizeof(char)*child_log_.cur_pos_;
  memcpy (&(dst_cl_ptr->child_log_.base_ptr_[dst_cl_ptr->child_log_.cur_pos_]), 
      child_log_.base_ptr_, size);
  dst_cl_ptr->child_log_.cur_pos_ += size;
  //LOG_DEBUG("After child queue data: child_log_.cur_pos_ = %ld\n", dst_cl_ptr->child_log_.cur_pos_);

  LOG_DEBUG("After packing: child_log_.size_= %ld\n", dst_cl_ptr->child_log_.size_);

  return kCommLogOK;
}


// input parameter
CommLogErrT CommLog::ReadData(void *buffer, uint64_t length)
{
  // reached end of logs, and need to return back and log data again...
  if (log_table_reexec_pos_ == log_table_.cur_pos_)
  {
    comm_log_mode_ = kGenerateLog;

    LOG_DEBUG("Reach end of log_table_ and comm_log_mode_ flip to %d\n", comm_log_mode_);
    return kCommLogCommLogModeFlip;
  }
  
  if (log_table_.cur_pos_ == 0)
  {
    ERROR_MESSAGE("Attempt to read from empty logs!\n");
    return kCommLogError;
  }

  if (length != log_table_.base_ptr_[log_table_reexec_pos_].length_)
  {
    ERROR_MESSAGE("Wrong length when read data from logs: %ld wanted but %ld expected!!\n", length, log_table_.base_ptr_[log_table_reexec_pos_].length_);
    return kCommLogError;
  }

  // if not completed, return kCommLogError, and needs to escalate
  if (log_table_.base_ptr_[log_table_reexec_pos_].completed_==false)
  {
    // Escalation to parent CD which is strict.
    // Report this event to head.
    ERROR_MESSAGE("Warning: Not completed non-blocking send function, needs to escalate...\n");
    //WARNING_MESSAGE("Not completed non-blocking send function, needs to escalate...\n");
    return kCommLogMissing;
  }

  if (length != 0)
  {
    memcpy(buffer, 
        log_queue_.base_ptr_+log_table_.base_ptr_[log_table_reexec_pos_].pos_,
        log_table_.base_ptr_[log_table_reexec_pos_].length_);
  }

  // check if current log entry is a repeated log entry
  if (log_table_.base_ptr_[log_table_reexec_pos_].isrepeated_)
  {
    log_table_.base_ptr_[log_table_reexec_pos_].reexec_counter_++;
    if (log_table_.base_ptr_[log_table_reexec_pos_].reexec_counter_ 
        == log_table_.base_ptr_[log_table_reexec_pos_].counter_)
    {
      LOG_DEBUG("ReadData to address (%p) and length(%ld)\n", buffer, length);
      LOG_DEBUG("reexec_pos_: %li\t cur_pos_: %li\n",log_table_reexec_pos_,log_table_.cur_pos_ );
      log_table_.base_ptr_[log_table_reexec_pos_].reexec_counter_ = 0;
      log_table_reexec_pos_++;
    }
  }
  else {
    LOG_DEBUG("ReadData to address (%p) and length(%ld)\n", buffer, length);
    LOG_DEBUG("reexec_pos_: %li\t cur_pos_: %li\n",log_table_reexec_pos_,log_table_.cur_pos_ );
    log_table_reexec_pos_++;
  }

  return kCommLogOK;
}


// input parameter
// differences between ProbeData and ReadData is 
//    1) ProbeData only check log entry, not copy data
//    2) ProbeData's buffer is const void *, since ProbeData does not need to change contents in buffer 
CommLogErrT CommLog::ProbeData(const void *buffer, uint64_t length)
{
  LOG_DEBUG("Inside ProbeData!\n");
  LOG_DEBUG("reexec_pos_: %li\t cur_pos_: %li\n",log_table_reexec_pos_,log_table_.cur_pos_ );
  // reached end of logs, and need to return back and log data again...
  if (log_table_reexec_pos_ == log_table_.cur_pos_)
  {
    comm_log_mode_ = kGenerateLog;

    LOG_DEBUG("Reach end of log_table_ and comm_log_mode_ flip to %d\n", comm_log_mode_);
    return kCommLogCommLogModeFlip;
  }
  
  if (log_table_.cur_pos_ == 0)
  {
    ERROR_MESSAGE("Attempt to read from empty logs!\n");
    return kCommLogError;
  }

  if (length != log_table_.base_ptr_[log_table_reexec_pos_].length_)
  {
    ERROR_MESSAGE("Wrong length when read data from logs: %ld wanted but %ld expected!!\n", length, log_table_.base_ptr_[log_table_reexec_pos_].length_);
    return kCommLogError;
  }

  // if not completed, return kCommLogError, and needs to escalate
  if (log_table_.base_ptr_[log_table_reexec_pos_].completed_==false)
  {
    WARNING_MESSAGE("Not completed non-blocking send function, needs to escalate...\n");
    return kCommLogMissing;
  }

  // check if current log entry is a repeated log entry
  if (log_table_.base_ptr_[log_table_reexec_pos_].isrepeated_)
  {
    log_table_.base_ptr_[log_table_reexec_pos_].reexec_counter_++;
    if (log_table_.base_ptr_[log_table_reexec_pos_].reexec_counter_ 
        == log_table_.base_ptr_[log_table_reexec_pos_].counter_)
    {
      log_table_.base_ptr_[log_table_reexec_pos_].reexec_counter_ = 0;
      log_table_reexec_pos_++;
    }
  }
  else {
    log_table_reexec_pos_++;
  }

  return kCommLogOK;
}

void CommLog::Print()
{
  my_cd_->GetCDID().Print();

  LOG_DEBUG("comm_log_mode_=%d\n", comm_log_mode_);

  LOG_DEBUG("Units:\n");
  LOG_DEBUG("queue_size_unit_ = %ld\n", queue_size_unit_);
  LOG_DEBUG("table_size_unit_ = %ld\n", table_size_unit_);
  LOG_DEBUG("child_log_size_unit_ = %ld\n", child_log_size_unit_);

  LOG_DEBUG("log_table_:\n");
  LOG_DEBUG("base_ptr_ = %p\n", &log_table_.base_ptr_[0]);
  LOG_DEBUG("cur_pos_ = %ld\n", log_table_.cur_pos_);
  LOG_DEBUG("table_size_ = %ld\n", log_table_.table_size_);
  
  uint64_t ii;
  for (ii=0;ii<log_table_.cur_pos_;ii++)
  {
    LOG_DEBUG("base_ptr_[%ld]: pos_=%ld, length_=%ld, completed_=%d, flag_=0x%x, counter_=%ld, reexec_counter_=%ld, isrepeated=%d\n\n"
        ,ii
        ,log_table_.base_ptr_[ii].pos_
        ,log_table_.base_ptr_[ii].length_
        ,log_table_.base_ptr_[ii].completed_
        ,log_table_.base_ptr_[ii].flag_
        ,log_table_.base_ptr_[ii].counter_
        ,log_table_.base_ptr_[ii].reexec_counter_
        ,log_table_.base_ptr_[ii].isrepeated_);
  }

  LOG_DEBUG ("log_table_reexec_pos_ = %ld\n", log_table_reexec_pos_);

  LOG_DEBUG("log_queue_:\n");
  LOG_DEBUG("base_ptr_ = %p\n", log_queue_.base_ptr_);
  LOG_DEBUG("cur_pos_ = %ld\n", log_queue_.cur_pos_);
  LOG_DEBUG("queue_size_ = %ld\n", log_queue_.queue_size_);

  LOG_DEBUG("child_log_:\n");
  LOG_DEBUG("base_ptr_ = %p\n", child_log_.base_ptr_);
  LOG_DEBUG("cur_pos_ = %ld\n", child_log_.cur_pos_);
  LOG_DEBUG("size_ = %ld\n", child_log_.size_);

  LOG_DEBUG("new_log_generated_ = %d\n\n", new_log_generated_);
}


CommLogErrT CommLog::UnpackLogsToChildCD(CD* child_cd)
{
  char *src_ptr;
  LOG_DEBUG("Unpack comm logs to child CD if there is any...\n");
  CommLogErrT ret = FindChildLogs(child_cd->GetCDID(), &src_ptr);
  if (ret != kCommLogOK)
  {
    WARNING_MESSAGE("\x1b[31mCould not find correspondent child logs, child may not push any logs yet...\x1b[0m\n");
    return ret;
  }

  (child_cd->comm_log_ptr_)->UnpackLogs(src_ptr);

  return kCommLogOK;
}

CommLogErrT CommLog::UnpackImplicitLogsToChildCD(CD* child_cd)
{
  char * src_ptr;
  LOG_DEBUG("Unpack implicit logs to child CD if there is any...\n");
  CommLogErrT ret = FindChildLogs(child_cd->GetCDID(), &src_ptr);
  if (ret != kCommLogOK)
  {
    //LOG_DEBUG("\x1b[31mCould not find correspondent IMPLICIT child logs, child may not push any logs yet...\x1b[0m\n");
    return ret;
  }

  (child_cd->implicit_comm_log_ptr_)->UnpackLogs(src_ptr);

  return kCommLogOK;
}

#ifdef libc_log
//GONG
CommLogErrT CommLog::UnpackLogsToChildCD_libc(CD* child_cd)
{
  char *src_ptr;
  LOG_DEBUG("Unpack libc logs to child CD if there is any...\n");
  CommLogErrT ret = FindChildLogs(child_cd->GetCDID(), &src_ptr);
  if (ret != kCommLogOK)
  {
    //LOG_DEBUG("\x1b[31mCould not find correspondent LIBC child logs, child may not push any logs yet...\x1b[0m\n");
    return ret;
  }

  (child_cd->libc_log_ptr_)->UnpackLogs(src_ptr);
  //GONG
//  child_cd->libc_log_ptr_->log_table_reexec_pos_ = 0;
  child_cd->libc_log_ptr_->Print();
  return kCommLogOK;
}
#endif

CommLogErrT CommLog::UnpackLogs(char *src_ptr, size_t len)
{
  //TODO: too many places return kCommLogError and with error messages
  // Either do it with one error return, and different messages, 
  // or different returns
  
  if (src_ptr == NULL) 
  {
    ERROR_MESSAGE("NULL src_ptr to unpack logs\n");
    return kCommLogError;
  }

  uint64_t length;
  uint64_t index;
  uint64_t size;

  //[SIZE]
  size = sizeof(uint64_t);
  memcpy (&length, src_ptr, size);
  if (len!=-1 && length!=len){
    ERROR_MESSAGE("Length in log (%lu) not equal to length requested (%lu).\n", length, len);
    return kCommLogError;
  }
  index = size;

  // [CDID]
  CDID cd_id;
  size = sizeof(CDID);
  memcpy(&cd_id, src_ptr+index, size);
  index += size;

  // [Table] meta data
  size = sizeof(uint64_t);
  uint64_t tmp;
  memcpy(&tmp, src_ptr+index, size);
  log_table_.cur_pos_ = tmp;
  index += size;
  memcpy(&tmp, src_ptr+index, size);
  log_table_.table_size_ = tmp;
  index += size;
  LOG_DEBUG("Unpack logs with table_size_ %ld, and cur_pos_ %ld\n", log_table_.table_size_, log_table_.cur_pos_);

  // [Table] data
  size = sizeof(struct LogTableElement)*log_table_.cur_pos_;
  if (log_table_.base_ptr_.size() != log_table_.table_size_)
    log_table_.base_ptr_.resize(log_table_.table_size_);
  memcpy(&log_table_.base_ptr_[0], src_ptr+index, size);
  index += size;

  // [Queue] meta data
  size = sizeof(struct LogQueue);
  memcpy(&log_queue_, src_ptr+index, size);
  index += size;

  // [Queue] data
  // FIXME: just increase queue size, not re-allocate spaces...
  char *tmp_queue_ptr = new char [log_queue_.queue_size_];
  cd::logging::tot_alloc_mem_ += log_queue_.queue_size_;
  if (tmp_queue_ptr == NULL)
  {
    ERROR_MESSAGE("Cannot allocate space for log_queue_.base_ptr_!\n");
    return kCommLogError;
  }
  //delete log_queue_.base_ptr_;
  log_queue_.base_ptr_ = tmp_queue_ptr;

  size = sizeof(char) *log_queue_.cur_pos_;
  memcpy(log_queue_.base_ptr_, src_ptr+index, size);
  index += size;

  // [ChildLogQueue] meta data
  size = sizeof(struct ChildLogQueue);
  memcpy(&child_log_, src_ptr+index, size);
  index += size;

  // [ChildLogQueue] data
  char *tmp_child_log_ptr = new char [child_log_.size_];
  cd::logging::tot_alloc_mem_ += child_log_.size_;
  if (tmp_child_log_ptr == NULL)
  {
    ERROR_MESSAGE("Cannot allocate space for child_log_.base_ptr_!\n");
    return kCommLogError;
  }
  //delete child_log_.base_ptr_;
  child_log_.base_ptr_ = tmp_child_log_ptr;

  size = sizeof(char) *child_log_.cur_pos_;
  memcpy(child_log_.base_ptr_, src_ptr+index, size);
  index += size;

  // sanity check
  if (index != length)
  {
    ERROR_MESSAGE("Something went wrong when unpacking data!\n");
    return kCommLogError;
  }
  
  return kCommLogOK;
}


CommLogErrT CommLog::FindChildLogs(CDID child_cd_id, char** src_ptr)
{
  uint64_t tmp_index=0;
  uint64_t length;
  CDID cd_id;
  #if _DEBUG
  //LOG_DEBUG("Inside FindChildLogs to print CDID wanted:\n");
  //child_cd_id.Print();
  #endif

  while(tmp_index < child_log_.cur_pos_)
  {
    memcpy(&length, child_log_.base_ptr_+tmp_index, sizeof(uint64_t));
    memcpy(&cd_id, child_log_.base_ptr_+tmp_index+sizeof(uint64_t), sizeof(CDID));
    #if CD_PGAS_ENABLED
    cd_id.ResetNodeIDPcolor();
    #endif

    #if _DEBUG
    //LOG_DEBUG("Temp CDID got:\n");
    //cd_id.Print();
    #endif
    if (cd_id == child_cd_id) 
    {
      LOG_DEBUG("Find the correct child logs\n");
      LOG_DEBUG("tmp_index=%ld, child_log_.cur_pos_=%ld\n", tmp_index, child_log_.cur_pos_);
      break;
    }
    tmp_index += length;
  }

  if (tmp_index >= child_log_.cur_pos_)
  {
    //WARNING_MESSAGE("Could not find correspondent child logs, child may not push any logs yet...\n");
    return kCommLogChildLogNotFound;
  }

  *src_ptr = child_log_.base_ptr_+tmp_index;

  return kCommLogOK;
}


// this function is used when a cd complete to prepare for next begin
void CommLog::Reset()
{
  log_table_.cur_pos_ = 0;
  log_queue_.cur_pos_ = 0;
  child_log_.cur_pos_ = 0;

  log_table_reexec_pos_ = 0;

  new_log_generated_ = false;

  comm_log_mode_ = kGenerateLog;
}

#endif
