//------------------------------------------------------------------------------------------------------------------------------
// Samuel Williams
// SWWilliams@lbl.gov
// Lawrence Berkeley National Lab
//------------------------------------------------------------------------------------------------------------------------------
#ifndef MG_H
#define MG_H
//------------------------------------------------------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
//------------------------------------------------------------------------------------------------------------------------------
#ifndef MG_AGGLOMERATION_START
#define MG_AGGLOMERATION_START  8 // i.e. start the distributed v-cycle when boxes are smaller than 8^3
#endif
#ifndef MG_DEFAULT_BOTTOM_NORM
#define MG_DEFAULT_BOTTOM_NORM  1e-3
#endif
//------------------------------------------------------------------------------------------------------------------------------
typedef struct {
  int num_ranks;	// total number of MPI ranks for MPI_COMM_WORLD
  int my_rank;		// my MPI rank for MPI_COMM_WORLD
  int       num_levels;	// depth of the v-cycle
#ifdef CDENABLED
  //std::vector<cd::cdvector<level_type> > levels;	// array of pointers to levels
  std::vector<level_type* > levels;	// array of pointers to levels
#else
  level_type ** levels;	// array of pointers to levels
#endif

  struct {
    uint64_t MGBuild; // total time spent building the coefficients...
    uint64_t MGSolve; // total time spent in MGSolve
  #ifdef CDENABLED
    friend class boost::serialization::access;
    template<class Archive>
      void serialize (Archive &ar, const unsigned int version){
        ar & MGBuild;
        ar & MGSolve;
      }
  #endif
  }cycles, cyclesMin;

  int MGSolves_performed;

  uint64_t bartime[200];

  int ncall[100];

#ifdef CDENABLED
  friend class boost::serialization::access;
  template<class Archive>
    void serialize (Archive &ar, const unsigned int version){
      ar &num_ranks;
      ar &my_rank; 
      ar &num_levels;
      ar &levels;
      ar &cycles;
      ar &cyclesMin;
      ar &MGSolves_performed;
      ar &bartime;
      ar &ncall;
    }
#endif

} mg_type;


//------------------------------------------------------------------------------------------------------------------------------
void  MGBuild(mg_type *all_grids, level_type *fine_grid, double a, double b, int minCoarseGridDim);
void  MGSolve(mg_type *all_grids, int u_id, int F_id, double a, double b, double desired_mg_norm);
void FMGSolve(mg_type *all_grids, int u_id, int F_id, double a, double b, double desired_mg_norm);
void MGPrintTiming(mg_type *all_grids);
void MGResetTimers(mg_type *all_grids);
//------------------------------------------------------------------------------------------------------------------------------
#endif
